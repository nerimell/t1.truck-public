function Cart() {
    this.productWords = ['товар', 'товара', 'товаров'];
    this.giftWords = ['подарок', 'подарка', 'подарков'];
    this.selectedDelivery = 0;
    this.init = function() {
        this.productsTable = $('#cart-products-table');
    };

    this.renderDbResult = function(db) {
        if(!db.cartPageInner) { return; }
        var el = document.getElementById('cart-products-table');
        var elSide = document.getElementById('cart-page-side');
        if(!el) { return; }
        el.innerHTML = db.cartPageInner;
        elSide.innerHTML = db.cartPageSide;
        lazyLoad(el);
    };

    this.selectAnotherGifts = function(el) {
        Actions.lockScreen();
        var giftsDialog = $('#gifts-dialog');
        setTimeout(function() {
            Actions.unlockScreen();
            giftsDialog.modal('show');
        }, 470);
        $el = $(el);
        var tr = $el.closest('tr');
        var giftsDataEl = tr.find('.product-gifts-json');
        var giftsValuesEl = tr.find('.product-gifts-values-json');
        try {
            var giftsData = JSON.parse(giftsDataEl.html());
            var giftsValues = JSON.parse(giftsValuesEl.html());
        } catch(e) {
            return;
        }
        var qty = tr.find('.quantity-control').val();
        giftsDialog.attr('data-maxgifts', qty).attr('data-hashid', tr.attr('id'));
        giftsDialog.find('#modal-desc-gifts-count').html(qty + ' ' +Actions.rusificate(qty, CartObject.giftWords[0], CartObject.giftWords[1], CartObject.giftWords[2]));
        var giftPrototype = $('#gift-prototype').clone();
        giftPrototype.attr('id', '');
        var giftsWrapper = $('#gifts-wrapper');
        var productWrapper = $('#checkout-modal-product');
        var product = {
            link: tr.find('a.product_title').attr('href'),
            title: tr.find('a.product_title').html(),
            avatar: tr.find('img').attr('src')
        };
        productWrapper.find('img').removeClass('not-found-image').attr('src', product.avatar);
        productWrapper.find('.checkout-modal-gift-title').html(product.title);
        productWrapper.find('.checkout-modal-gift-link').attr('href', product.link);
        giftsWrapper.html('');
        var used = 0;
        for(var i = 0; i < giftsData.length; i++) {
            var newGift = giftPrototype.clone();
            if(giftsValues[giftsData[i].product_id]) {
                if(summFloat(giftsValues[giftsData[i].product_id], used) <= qty) {
                    newGift.find('.quantity-control').val(giftsValues[giftsData[i].product_id]).attr('data-product', giftsData[i].product_id);
                    used+=giftsValues[giftsData[i].product_id];
                } else {
                    newGift.find('.quantity-control').val(0).attr('data-product', giftsData[i].product_id);
                }
            } else {
                newGift.find('.quantity-control').val(0).attr('data-product', giftsData[i].product_id);
            }
            newGift.find('img').removeClass('not-found-image').attr('src', giftsData[i].avatar);
            newGift.find('.checkout-modal-gift-title').html(giftsData[i].title);
            newGift.find('.checkout-modal-gift-link').attr('href', giftsData[i].link);
            newGift.find('.product-total-no-discount').html(giftsData[i].priceinfo.final_price);
            giftsWrapper.append(newGift);
        }
        if(used < qty) {
            var el = giftsWrapper.find('.checkout-modal-gift')[0].getElementsByClassName('quantity-control')[0];
            el.value = parseInt(summFloat(el.value, qty, (used*-1)));
        }
    };


    this.changeQuantity = function(event, el) {
        var qtyEl = $(el);
        var qty = qtyEl.val();
        if(isNaN(qty)) { qtyEl.val(1); qty = 1; }
        var tr = qtyEl.closest('tr');
        CartObject.setGiftWords(tr, qty);
        CartObject.recalculate();
        return true;
    };

    this.changeQuantityAsync = function(event, el) {
        if(!validateNumericInput(event)) { return false; }
        if(window.changeQuantityTimeout) {
            clearTimeout(window.changeQuantityTimeout);
        }
        window.changeQuantityTimeout = setTimeout(function() {
            CartObject.changeQuantityAsyncQuery(event, el);
        }, 500);
        return true;
    };

    this.changeQuantityAsyncQuery = function(ev, el) {
            var qtyEl = $(el);
            var qty = qtyEl.val();
            if(isNaN(qty)) { qtyEl.val(1); qty = 1; }
            Actions.lockScreen();
            var tr = el.closest('tr');
            var data = { quantity: qty, cartPage: 1, order_city: CartObject.getCity() };
            if(tr.dataset.type == 'p') {
                var url = '/cart/set';
                data.product_id = tr.dataset.id;
            } else {
                var url = '/cart/set-complex';
                data.complex_id = tr.dataset.id;
            }
            $.ajax({
                url: url,
                data: data,
                type: 'post',
                dataType: 'json',
                success: function(db) {
                    Actions.unlockScreen();
                    CartObject.renderDbResult(db);
                    Actions.message(db.display);
                }, error: function() {
                    Actions.unlockScreen();
                }
            });
    };

    this.setGiftWords = function($el, qty) {
        var selectorEl = $el.find('.checkout-products-gifts-select-another');
        var variant = qty > 1 ? 'b' : 'a';
        selectorEl.html(selectorEl.attr('data-'+variant));
        $el.find('.checkout-products-gifts-word').html(Actions.rusificate(qty, this.giftWords[0], this.giftWords[1], this.giftWords[2]));
        $el.find('.checkout-products-gifts-count').html(qty);
    };

    this.coupon = {
        activate: function(el) {
            Actions.coupon.activate(el, CartObject.coupon.callback);
            return false;
        },
        callback: function(db) {
            document.getElementById('current-coupon-percent').innerHTML = db.coupon.discount_value;

            CartObject.recalculateData();
        }
    };

    this.toggleGifts = function(giftsCount) {
        if(giftsCount) {
            $('.gifts-info-wrapper').slideDown();
        } else {
            $('.gifts-info-wrapper').slideUp();
        }
    };

    this.fillRecalculatedData = function(db) {
        if(db.coupon) {
            $('.coupon-info-wrapper').slideDown();
        } else {
            $('.coupon-info-wrapper').slideUp();
        }

        for(var i = 0; i < db.cart.instances.length; i++) {
            var price = JSON.stringify(db.cart.instances[i].priceinfo);
            if(db.cart.instances[i].complex_id) {
                $('#complex-'+db.cart.instances[i].complex_id).find('.product-data-json').html(price);
                continue;
            }
            $('#instance-'+db.cart.instances[i].product_id).find('.product-data-json').html(price);
        }
        this.getRows();
        this.recalculate();

    };

    this.recalculateData = function() {
        $.ajax({
            url: '/cart/calculate-data',
            data: { order_city: CartObject.getCity(), cartPage: 1 },
            beforeSend: function() {
                Actions.lockScreen();
            },
            type: 'post',
            dataType: 'json',
            success: function(db) {
                if(db.result) {
                    console.log(db);
                    CartObject.renderDbResult(db);
                } else {
                    Actions.error(db.display);
                }
                Actions.unlockScreen();
            },
            error: function(a, b, c) {
                Actions.unlockScreen();
            }
        });
    };
    this.cartCityCallback = function(db) {
        setTimeout(function() {
            CartObject.recalculateData();
            CartObject.calculateDeliveries();
        }, 300);
    };



    this.step = function(arg) {
      $('#'+arg).trigger('click');
        CartObject.calculateDeliveries();
      setTimeout(function() {
        $('html,body').animate({ scrollTop: 0 }, 400);
      }, 350);
    };

    this.changeDelivery = function(el) {
        if(el.checked) {
            var addressField = document.getElementById('address-field');
            if(el.dataset.requireaddress) {
                addressField.dataset.required = 1;
                validator.validateField(addressField);
            } else {
                delete addressField.dataset.required;
                validator.validateField(addressField);
            }
            CartObject.selectedDelivery = el.id;
        }
    };

    this.forceChangeDelivery = function(id) {
        var el = document.getElementById(id);
        el.checked = true;
        CartObject.changeDelivery(el);
    };

    this.getProductsFormData = function() {
        var data = {
            instances: [],
            coupon_id: document.getElementById('active-coupon-id').value,
            city_id: document.getElementsByClassName('hidden-city')[0].value
        };
        var rows = $('.instance-tr');
        for(var i = 0; i < rows.length; i++) {
            var tr = $(rows[i]);
            var instance = {
                type: tr.attr('data-type'),
                instanceId: tr.attr('data-id'),
                quantity: tr.find('.quantity-control').val(),
                gifts: 0,
                priceinfo: JSON.parse(tr.find('.product-data-json').html())
            };
            var giftsContainer = $('.product-gifts-values-json');
            if(giftsContainer.length) {
                instance.gifts = JSON.parse(giftsContainer.html());
            }
            data.instances.push(instance);
        }
        return data;
    };

    this.calculateDeliveries = function() {
        var data = CartObject.getProductsFormData();
        console.log('asd');
        $.ajax({
            url: '/delivery/cart',
            data: data,
            type: 'post',
            dataType: 'json',
            success: function(db) {
                cartTotal = db.cart.totals.final_price;
                Actions.product.fillDeliveries(db);
                Actions.product.highlightDeliveries();
                var freeDeliveryEl = document.getElementById('free-delivery-container');
                if(freeDeliveryEl == null) { return; }
                if(Actions.product.deliveryIsFree()) {
                    freeDeliveryEl.style.display = 'block';
                    if(!CartObject.selectedDelivery) {
                        CartObject.forceChangeDelivery('free-terminal-truck-input');
                    }
                } else {
                    freeDeliveryEl.style.display = 'none';
                    var id = 'free-terminal-truck-input';
                    if(CartObject.selectedDelivery == id) {
                        CartObject.selectedDelivery = false;
                        document.getElementById('address-field').dataset.required = 1;
                    }
                    document.getElementById(id).checked = false;
                }
            },
            error: function(a,b,c) {

            }
        });
    };

    this.getUserData = function() {
        var form = $('#userform');
        return {
            last_name: form.find('input[name="last_name"]').val(),
            first_name: form.find('input[name="first_name"]').val(),
            third_name: form.find('input[name="third_name"]').val(),
            phone: form.find('input[name="phone"]').val(),
            address: form.find('input[name="address"]').val(),
            post_index: form.find('input[name="post_index"]').val(),
            user_type: form.find('select[name="user_type"]').val(),
            company_name: form.find('input[name="company_name"]').val(),
            checking_account: form.find('input[name="checking_account"]').val(),
            legal_address: form.find('input[name="legal_address"]').val(),
            bank_name: form.find('input[name="bank_name"]').val(),
            city: form.find('input[name="city"]').val(),
            inn: form.find('input[name="inn"]').val(),
            kpp: form.find('input[name="kpp"]').val(),
            okpo: form.find('input[name="okpo"]').val(),
            bik: form.find('input[name="bik"]').val(),
            correspond_account: form.find('input[name="correspond_account"]').val(),
            company_director: form.find('select[name="company_director"]').val(),
            company_director_based_on: form.find('select[name="company_director_based_on"]').val(),
            trusted_person: form.find('input[name="trusted_person"]').val(),
            email: form.find('input[name="email"]').val(),
        };
    };

    this.getSelectedDelivery = function() {
        try {
            if(CartObject.selectedDelivery) {
                return document.getElementById(CartObject.selectedDelivery).value;
            }
            return 0;
        } catch(e) {
            return 0;
        }
    };

    this.getOrderData = function() {
        return { couponCode: $('#coupon-code-input').val(), orderId: $('#order-id').val(), totals: JSON.stringify(CartObject.recalculateRows()), data: JSON.stringify(CartObject.getProductsFormData()), user: JSON.stringify(CartObject.getUserData()), delivery: CartObject.getSelectedDelivery() };
    };

    this.getPaymentMethod = function() {
        return $('input[name="payment"]:checked').val();
    };

    this.checkAnOrder = function() {
        $.ajax({
            url: '/cart/validate-order',
            type: 'post',
            dataType: 'json',
            data: { cartPage: 1, order_city: CartObject.getCity(), delivery: CartObject.getSelectedDelivery(), payment: CartObject.getPaymentMethod(), user: JSON.stringify(CartObject.getUserData()) },
            async: false,
            success: function(db) {
                if(db.result) {
                    if(db.cart.hash != document.getElementById('order-hash-summ').value) {
                        CartObject.renderDbResult(db);
                        Actions.confirm({ simple: 1, msg: 'Заказ был изменен, перепроверьте его пожалуйста' });
                        CartObject.step('step-1-btn');
                    } else {
                        if(db.payment == 0 || !db.payment) {
                            var nw = window.open(db.paymentUrl, '_blank');
                            nw.location.href = db.paymentUrl;
                        } else {
                            var form = document.getElementById('order-form');
                            document.getElementById('order-hash').value = db.hash;
                            form.submit();
                        }
                    }
                } else {
                    Actions.confirm({ simple: 1, title: 'Предупреждение', msg: db.display });
                }
            }, error: function(a, b, c) {

            }
        });
    };




    this.checkout = function(el) {
        if(el.dataset.valid == 1) { return true; }
        var productsCheckoutTabHeader = document.getElementById('products-checkout-tab-header');
        if(productsCheckoutTabHeader == null) { return false; }
        if(productsCheckoutTabHeader.parentNode.classList.contains('active')) { // redirect into profile tab
            var profileCheckoutTabHeader = document.getElementById('profile-checkout-tab-header');
            if(profileCheckoutTabHeader == null) { return false; }
            profileCheckoutTabHeader.click();
            return false;
        }
        if(!this.validateCheckout()) { return false; }
        return false;
    };

    this.getProducts = function() {
        var productRows = $('#cart-products-table').find('tr');
        var products = [];
        productRows.each(function() {
            var el = $(this);
            products.push({ product_id: el.find('.product_id').val(), quantity: el.find('.quantity-control').val() });
        });
        return products;
    };

    this.validateCheckout = function() {
        var form = document.getElementById('checkout-form');
        var formData = new FormData(form);
        formData.append('products', JSON.stringify(this.getProducts()));
        $.ajax({
            url: '/cart/validate',
            data: formData,
            type: 'post',
            dataType: 'json',
            cache: false,
            processData: false,
            contentType: false,
            success: function(db) {
                if(db.result) {
                    form.dataset.valid = 1;
                    form.submit();
                } else {
                    Actions.error(db.display);
                }
            }
        });
    };


    this.quantityPlus = function(el) {
        var $el = $(el);
        var qtyEl = $el.closest('.checkout-quantity').find('.quantity-control');
        var qty = qtyEl.val();
        var nqty = summFloat(qty, 1);
        if(nqty > 99) { nqty = 99; }
        if(nqty < 1) { nqty = 1; }
        if(isNaN(nqty)) {
            nqty = 1;
        }
        var tr = el.closest('tr');
        var data = { quantity: nqty, cartPage: 1, order_city: CartObject.getCity() };
        if(tr.dataset.type == 'p') {
            var url = '/cart/set';
            data.product_id = tr.dataset.id;
        } else {
            var url = '/cart/set-complex';
            data.complex_id = tr.dataset.id;
        }
        Actions.lockScreen();
        $.ajax({
            url: url,
            data: data,
            type: 'post',
            dataType: 'json',
            success: function(db) {
                Actions.unlockScreen();
                CartObject.renderDbResult(db);
                Actions.message(db.display);
            }, error: function() {
                Actions.unlockScreen();
            }
        });
    };

    this.giftQuantityPlus = function(el) {
        var giftsTotal = CartObject.countModalGifts();
        if(giftsTotal >= document.getElementById('gifts-dialog').dataset.maxgifts) { return; }
        var $el = $(el);
        var qtyEl = $el.closest('.checkout-quantity').find('.quantity-control');
        var qty = qtyEl.val();
        var nqty = summFloat(qty, 1);

        if(nqty > 99) { nqty = 99; }
        if(nqty < 0) { nqty = 0; }
        if(isNaN(nqty)) {
            nqty = 1;
        }
        qtyEl.val(nqty);
        CartObject.setGiftWords($el.closest('tr'), nqty);
    };

    this.giftQuantityMinus = function(el) {
        var $el = $(el);
        var qtyEl = $el.closest('.checkout-quantity').find('.quantity-control');
        var qty = qtyEl.val();
        var nqty = summFloat(qty, -1);
        if(nqty > 99) { nqty = 99; }
        if(nqty < 0) { nqty = 0; }
        if(isNaN(nqty)) {
            nqty = 1;
        }
        qtyEl.val(nqty);
        CartObject.setGiftWords($el.closest('tr'), nqty);
    };

    this.countModalGifts = function() {
        var totalGifts = 0;
        var giftsDialog = $('#gifts-dialog');
        giftsDialog.find('.quantity-control').each(function() {
            totalGifts+=parseInt(this.value);
        });
        return totalGifts;
    };

    this.getCity = function() {
        var el = document.getElementById('fields-city');
        if(!el) { return 0; }
        return el.value;
    };

    this.saveGiftQuantity = function() {
        var giftsDialog = $('#gifts-dialog');
        var instance = $('#'+giftsDialog.attr('data-hashid'));
        if(!instance.length) { giftsDialog.modal('close'); }
        var max = giftsDialog.attr('data-maxgifts');
        var totalGifts = 0;
        var data = {};
        giftsDialog.find('.quantity-control').each(function() {
            var value = parseInt(this.value);
            totalGifts += value;
            data[this.dataset.product] = value;
        });

        if(totalGifts < max) { errorContainerAction(giftsDialog, 'Вам надо выбрать '+max+' '+Actions.rusificate(max, this.giftWords[0], this.giftWords[1], this.giftWords[2]) + ' чтобы применить изменения'); return; }
        if(totalGifts > max) { errorContainerAction(giftsDialog, 'Вам надо выбрать до '+max+' '+Actions.rusificate(max, this.giftWords[0], this.giftWords[2], this.giftWords[2]) + ' чтобы применить изменения'); return; }
        $.ajax({
            url: '/cart/set-gifts',
            type: 'post',
            dataType: 'json',
            beforeSend: function() {
                Actions.lockScreen();
            },
            data: { giftsData: JSON.stringify(data), product_id: instance.attr('data-id'), cartPage: 1, order_city: CartObject.getCity()  },
            success: function(db) {
                Actions.unlockScreen();
                CartObject.renderDbResult(db);
                giftsDialog.find('.modal-close-btn').click();
            }, error: function() {
                Actions.unlockScreen();
            }
        });

    };

    this.rememberUserField = function(el) {
        var data = { name: el.name, value: el.value };
        $.ajax({
            url: '/user/remember-field',
            type: 'post',
            dataType: 'json',
            data: data,
            success: function(db) {

            }
        });
    };

    this.quantityMinus = function(el) {
        var $el = $(el);
        var qtyEl = $el.closest('.checkout-quantity').find('.quantity-control');
        var qty = qtyEl.val();
        var nqty = summFloat(qty, -1);
        if(nqty > 99) { nqty = 99; }
        if(nqty < 1) { nqty = 1; }
        if(isNaN(nqty)) {
            nqty = 1;
        }
        Actions.lockScreen();
        var tr = el.closest('tr');
        var data = { quantity: nqty, cartPage: 1, order_city: CartObject.getCity() };
        if(tr.dataset.type == 'p') {
            var url = '/cart/set';
            data.product_id = tr.dataset.id;
        } else {
            var url = '/cart/set-complex';
            data.complex_id = tr.dataset.id;
        }
        $.ajax({
            url: url,
            data: data,
            type: 'post',
            dataType: 'json',
            success: function(db) {
                Actions.unlockScreen();
                CartObject.renderDbResult(db);
                Actions.message(db.display);
            }, error: function() {
                Actions.unlockScreen();
            }
        });
    };


}
var CartObject = new Cart;
CartObject.init();
Actions.location.callback = CartObject.cartCityCallback;
Actions.product.hideDelivery = 0;
Actions.cartPage = 1;
document.getElementById('step-2-btn').addEventListener('click', CartObject.calculateDeliveries);
document.getElementById('step-1-btn').addEventListener('click', CartObject.recalculateData);
var fields = $('input[type="text"].validator');
fields.on('input', function() {
    validator.validateField(this);
});


$('.remember').on('change', function() {
    CartObject.rememberUserField(this);
});
