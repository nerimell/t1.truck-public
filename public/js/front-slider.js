var frontSlider = {
    frontPageSlider: null,
    slidesInfo: null,
    slidesInfoObject: null,
    imageContainer: null,
    pagination: null,
    currentItem: -1,
    interval: 1000000,
    intervalFn: null,
    currentContainer: 0,
    previousContainer: 1,
    zIndex: 2,
    blocked: 0,
    sizes: {
        width: 0,
        height: 0
    },
    elements: [{}, {}],
    init: function() {
        this.frontPageSlider = document.getElementById('front-page-slider');
        this.pagination = document.getElementById('front-page-slider-pagination').getElementsByTagName('li');
        this.imageContainer = document.getElementById('front-page-slider-image-container');
        this.collectSlidesInfo();
        if(!this.frontPageSlider || !this.slidesInfoObject || !this.slidesInfoObject.length) { return; }
        this.elements[0].image = document.getElementById('front-page-slider-image-container');
        this.elements[0].title = document.getElementById('front-page-slider-title');
        this.elements[0].description = document.getElementById('front-page-slider-description');
        this.elements[0].readmoreContainer = document.getElementById('front-page-slider-readmore');
        this.elements[0].readmorebtn = document.getElementById('front-page-slider-readmore-btn');
        this.elements[0].priceEl = document.getElementById('front-page-slider-price');
        this.elements[0].content = document.getElementById('front-page-slider-content');
        this.elements[1].title = document.getElementById('front-page-slider-title-2');
        this.elements[1].description = document.getElementById('front-page-slider-description-2');
        this.elements[1].readmoreContainer = document.getElementById('front-page-slider-readmore-2');
        this.elements[1].readmorebtn = document.getElementById('front-page-slider-readmore-btn-2');
        this.elements[1].priceEl = document.getElementById('front-page-slider-price-2');
        this.elements[1].content = document.getElementById('front-page-slider-content-2');
        this.recalculateSizes();
        var that = this;
        frontSlider.toggleNextItem();
        this.next(1);
        this.intervalFn = setInterval(function() {
            frontSlider.toggleNextItem();
            that.next();
        }, this.interval);
    },
    recalculateSizes: function() {
        this.sizes.width = this.elements[0].image.clientWidth;
        this.sizes.height = this.elements[0].image.clientHeight;
    },
    toggleNextItem: function() {
        this.currentItem = (typeof this.slidesInfoObject[this.currentItem+1] == 'undefined') ? 0 : this.currentItem+1;
    },
    next: function(ignore) {
        this.markPagination();
        this.renderItem();
        this.blocked = 1;
        if(!ignore) {
            this.elements[this.currentContainer].content.style.right = '-400px';
            setTimeout(function() {
                frontSlider.elements[frontSlider.previousContainer].content.style.right = '95px';
                setTimeout(function() {
                    frontSlider.elements[frontSlider.previousContainer].content.style.right = '0';
                    frontSlider.blocked = 0;
                }, 260);
            }, 400);
        } else {
            frontSlider.blocked = 0;
        }
    },
    goTo: function(number) {
        if(this.blocked) { return; }
        if(number == this.currentItem) { return; }
        this.currentItem = (typeof this.slidesInfoObject[number] == 'undefined') ? 0 : number;
        this.markPagination();
        this.next();
    },
    markPagination: function() {
        for(var i = 0; i < this.pagination.length; i++) {
            if(i != this.currentItem) {
                this.pagination[i].classList.remove('active');
                continue;
            }
            this.pagination[i].classList.add('active');
        }
    },
    switchRenderingContainer: function() {
        this.previousContainer = this.currentContainer ? 1 : 0;
        this.currentContainer = this.currentContainer ? 0 : 1;
    },
    prev: function() {
        this.currentItem = (typeof this.slidesInfoObject[this.currentItem-1] == 'undefined') ? this.slidesInfoObject.length-1 : this.currentItem-1;
        this.renderItem();
    },
    renderSlide: function() {
        var background = 'url('+this.slidesInfoObject[this.currentItem].image+')';
        var x = 0;
        var offset = 20;
        var elementsCount = 100/offset;
        var y = 0;
        this.elements[0].image.innerHTML = '';
        var itemWidth = this.sizes.width/elementsCount;
        var bgSize = this.sizes.width+'px '+this.sizes.height+'px';
        var time = 450;
        for(var i = 0; i < 5; i++) {
            y = (itemWidth*i);
            var div = document.createElement('div');
            div.style.backgroundImage = background;
            div.style.backgroundPosition = -y+'px '+x+'px';
            div.style.backgroundSize = bgSize;
            div.style.width = itemWidth+'px';
            div.style.height = '100%';
            div.style.position = 'absolute';
            if((i%2) == 0) {
                div.style.top = '-100%';
            } else {
                div.style.bottom = '-100%';
            }
            div.style.left = y+'px';
            div.style.zIndex = this.zIndex;
            this.elements[0].image.append(div);
        }
        for(i = 0; i < this.elements[0].image.childNodes.length; i++) {
            var params = {};
            if((i%2) == 0) {
                params.top = 0;
            } else {
                params.bottom = 0;
            }
            $(this.elements[0].image.childNodes[i]).animate(params, time);
        }
        setTimeout(function() {
            frontSlider.elements[0].image.style.backgroundImage = background;
            frontSlider.elements[0].image.style.backgroundSize = '100% 100%';
        }, 400);

        this.zIndex++;
    },
    renderItem: function() {
        this.renderSlide();
        var title = this.slidesInfoObject[this.currentItem].title.toString().trim().length ? (this.slidesInfoObject[this.currentItem].link.toString().trim().length ? '<a href="'+this.slidesInfoObject[this.currentItem].link+'">'+this.slidesInfoObject[this.currentItem].title+'</a>' : '<span class="type-text">'+this.slidesInfoObject[this.currentItem].title+'</span>') : '';
        if(title) {
            this.elements[this.currentContainer].title.innerHTML = title;
        }
        this.elements[this.currentContainer].description.innerHTML = this.slidesInfoObject[this.currentItem].description ? this.slidesInfoObject[this.currentItem].description : '';
        if(this.slidesInfoObject[this.currentItem].link && this.slidesInfoObject[this.currentItem].showreadmore == 1) {
            this.elements[this.currentContainer].readmorebtn.href = this.slidesInfoObject[this.currentItem].link;
            this.elements[this.currentContainer].readmorebtn.classList.remove('hidden');
        } else {
            this.elements[this.currentContainer].readmorebtn.classList.add('hidden');
        }
        if(this.slidesInfoObject[this.currentItem].price) {
            this.elements[this.currentContainer].priceEl.innerHTML = this.slidesInfoObject[this.currentItem].price;
            this.elements[this.currentContainer].priceEl.classList.remove('hidden');
        } else {
            this.elements[this.currentContainer].priceEl.classList.add('hidden');
        }
        frontSlider.switchRenderingContainer();
    },
    collectSlidesInfo: function() {
        this.slidesInfo = document.getElementById('slides-info');
        if(!this.slidesInfo) { return; }
        var els = this.slidesInfo.getElementsByClassName('item');
        this.slidesInfoObject = [];
        for(var i = 0; i < els.length; i++) {
            this.slidesInfoObject.push({
                    title: els[i].getElementsByClassName('title')[0].value,
                    description: els[i].getElementsByClassName('description')[0].value.trim(),
                    showreadmore: els[i].getElementsByClassName('showreadmore')[0].value,
                    link: els[i].getElementsByClassName('link')[0].value,
                    image: els[i].getElementsByClassName('img')[0].value,
                    price: els[i].getElementsByClassName('price')[0].value
            });
        }
    }
};
frontSlider.init();