body {
    font-family: DejaVu Sans, sans-serif;
    font-size: 11px;
}

a {
    text-decoration: none;
}

.text-center {
    text-align: center;
}

.heading {
    font-weight: bold;
    font-size: 13px;
    padding: 20px 20px 35px;
    letter-spacing: 2px;
}

li {
    list-style-type: none;
}

ol {
    counter-reset: punkt;
    margin-left: 0;
    padding-left: 0;
}

ol li:before {
    counter-increment: punkt 1;
    content: counter(punkt) ". ";
}

ol ol {
    counter-reset: podpunkt;
    padding-left: 25px;
}

ol ol li:before {
    counter-increment: podpunkt;
    content: counter(punkt) "." counter(podpunkt) ". ";
}

.dogovor td, .dogovor th {
    font: normal 12px/18px Times, 'Times New Roman', serif;
}

table.dogovor, .dogovor tr, .dogovor td {
    border: 0 !important
}

table.dogovorborder {
    border-spacing: 0;
    border-collapse: collapse
}

.dogovor .dogovorborder tr, .dogovor .dogovorborder td, .dogovor .dogovorborder th {

}

.clear-paragraph {
    margin: 0;
}

.z-index-10 {
    position: relative;
    z-index: 10;
}

.relative {
    position: relative;
}

.contract-products th, .contract-products td, .contract-products tr {
    border: 1px solid #000 !important;
    padding: 5px 3px;;
    border-spacing: 0 !important;
    border-collapse: collapse
}

.contract-products td {
    padding: 3px 5px;
}

.contract-products {
    margin: 30px 0;
    width: 100%;
}