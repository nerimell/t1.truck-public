<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCatalogRows extends Migration{

    public $catalog;

    function __construct() {

        // quicksilver
    }

    public function up(){
        $this->catalog = [];
        $catalog[] = ['category_id' => 444, 'title' => 'Официальный каталог запчастей Mercury / Mercruiser', 'css' => 'ping-bg', 'alias' => 'catalog'];
        $catalog[] = ['category_id' => 445, 'parent' => 444, 'title' => 'Подвесные моторы Mercury',  'alias' => 'mercury'];
        $catalog[] = ['category_id' => 446, 'parent' => 444, 'title' => 'Бензиновые стационарные двигатели MerCruiser',  'alias' => 'mercruiser-benzin'];
        $catalog[] = ['category_id' => 447, 'parent' => 444, 'title' => 'Поворотно-откидные колонки MerCruiser Alpha и MerCruiser Bravo', 'alias' => 'mercruiser-alpha-bravo'];
        $catalog[] = ['category_id' => 447, 'parent' => 444, 'title' => 'Дизельные стационарные двигатели MerCruiser', 'alias' => 'mercruiser-disel'];
        \DB::table('categories')
         ->insert($this->catalog);
    }


    public function down(){
        $ids = [];
        foreach($this->catalog as $x) {
            $ids[] = $x->category_id;
        }

        \DB::table('categories')->whereIn('category_id', $ids)->delete();
    }
}
