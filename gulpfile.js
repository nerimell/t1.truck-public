const elixir = require('laravel-elixir');

process.env.DISABLE_NOTIFIER = true
elixir(mix => {
    mix.sass([
    'fonts.scss',
    'animations.scss',
    'bootstrap.scss',
    'all.scss',
    'style.scss',
    'responsive.scss',
    'toastr.scss'
],  'public/css')


});