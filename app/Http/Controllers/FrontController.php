<?php


namespace App\Http\Controllers;
use App\Models\ShopModel;
use App\Models\SettingsModel;

class FrontController extends Controller {

    public $model;

    function page($arg = false) {
        if($arg) { $this->pageInfo($arg); }
    }

    function view($path, $args = []) {
        (SettingsModel::getInstance())->shareSiteData($this->pageInfo);
        $shopModel = ShopModel::getInstance();
        $shopModel->getCart();
        \View::share(['menu' => \json_decode(file_get_contents('../resources/data/menu.json')), 'cart' => $shopModel->cart, 'coupon' => ShopModel::getActiveCoupon()]);
        return \view($path, $args);
    }

}