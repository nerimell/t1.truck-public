<?php

namespace App\Http\Controllers;
use App\Models\UsersModel;
use App\Models\ShopModel;

class UsersController extends FrontController  {

    function __construct() {
        $this->model = UsersModel::getInstance();
    }

    function index() {
        $this->page();
        return view('users.index');
    }

    function my() {
        return \redirect()->to('/profile');
    }

    function registrationPage() {
        $this->page((object)['meta_title' => 'Регистрация']);
        $breadcrumbs = [(object)['link' => '/registration', 'title' => 'Регистрация']];
        $terms = file_get_contents('../resources/data/terms.html');
        return $this->view('users.registration', compact('breadcrumbs', 'terms'));
    }

    function loginPage() {
        $this->page((object)['meta_title' => 'Авторизация']);
        $breadcrumbs = [(object)['link' => '/login', 'title' => 'Авторизация']];
        return $this->view('users.login', compact('breadcrumbs'));
    }

    function restorePage() {
        $this->page((object)['meta_title' => 'Восстановление пароля']);
        $breadcrumbs = [(object)['link' => '/restore-password', 'title' => 'Восстановление пароля']];
        return $this->view('users.restore', compact('breadcrumbs'));
    }

    function restoreLoginPage() {
        $this->page((object)['meta_title' => 'Восстановление логина']);
        $breadcrumbs = [(object)['link' => '/restore-login', 'title' => 'Восстановление логина']];
        return $this->view('users.restore-login', compact('breadcrumbs'));
    }

    function profilePage() {
        $this->page((object)['meta_title' => 'Личный кабинет']);
        $breadcrumbs = [(object)['link' => '/profile', 'title' => 'Личный кабинет']];
        $usertypes = $this->model->getUserTypes();
        $userData = $this->model->getUser(logged);
        $userFields = $userData;
        $require = 0;
        $dialogCallback = 'Actions.location.profileCityCallback';
        return $this->view('users.profile', compact('breadcrumbs', 'require', 'usertypes',
         'dialogCallback', 'userData', 'userFields'));
    }

    function myOrders() {
        $this->page((object)['meta_title' => 'Мои заказы']);
        $breadcrumbs = [(object)['link' => '/my/offers', 'title' => 'Мои заказы']];
        $shopModel = ShopModel::getInstance();
        $orders = $shopModel->getUserOrders(logged);
        return $this->view('users.orders', compact('breadcrumbs', 'orders'));
    }

    function asyncLogin() {
        return \json_encode($this->model->login());
    }

    function logout(\Request $request) {
        \Auth::logout();
        return redirect()->back();
    }

    function register() {
        return \json_encode($this->model->register());
    }

    function saveSettings() {
        return \json_encode($this->model->saveSettings());
    }

    function rememberField() {
        $name = trim(\Request::get('name'));
        $value = trim(\Request::get('value'));
        $user = $this->model->getUser(logged);
        if(!$name || $value == '') { return; }
        foreach($user as $k => $v) {
            if($k == $name) {
                \Cookie::queue(\Cookie::make($name, $value, time()+2678400));
            }
        }

        return '';
    }
}
