<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\SettingsModel;

class Controller extends BaseController
{

    public $pageInfo;
    public $model;



    function pageInfo($arg) {
        $this->pageInfo = new \stdCLass;
        $this->pageInfo->meta_title = @$arg->meta_title ? $arg->meta_title : @$arg->title;
        $this->pageInfo->meta_description = @$arg->meta_description ? $arg->meta_description : @$arg->description;
        $this->pageInfo->meta_keywrods = @$arg->meta_keywords ? @$arg->meta_keywords : '';
    }




    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
