<?php

namespace App\Http\Controllers;
use App\Models\ProductsModel;

class DoublesController extends FrontController {

    public $productsModel;
    public $pair;

    function __construct() {
        $this->productsModel = ProductsModel::getInstance();
        define('debug', 1);
    }

    function doublesPage() {
        $user = \Auth::user();
        if(@$user->username != 'xqfce' && @$user->username != 'xqfce1') {
          //  die('permission denied');
        }

        $this->recursiveGet();
        $view = \Request::ajax() ? 'dev-products' : 'dev';
        return $this->view($view, ['a' => $this->a, 'b' => $this->b, 'code' => $this->code]);
    }

    function recursiveGet() {
        $doubles = $this->getDoubles();
        if(!$this->productsModel->getProductById($doubles->a)) { $this->pair = $doubles; $this->deletePair(); return $this->recursiveGet(); }
        $this->a = clone $this->productsModel->product;
        if(!$this->productsModel->getProductById($doubles->b)) { $this->pair = $doubles; $this->deletePair(); return $this->recursiveGet(); }
        $this->b = clone $this->productsModel->product;
        if($this->a->ptr && $this->b->ptr) {
            $this->pair = $doubles; $this->deletePair(); return $this->recursiveGet();
        }
        if($this->a->product_id == $this->b->product_id) {
            $this->pair = $doubles; $this->deletePair(); return $this->recursiveGet();
        }
        handleProductLink($this->a);
        handleProductLink($this->b);
        $this->code = $doubles->code;
    }

    function intoProduct() {
        $this->getRequestPair();
        $this->winner = \Request::get('winner');
        $this->result = new \Result;
        if(!$this->pair->a || !$this->pair->b || !$this->pair->code) { $this->result->display = 'слияние не произведено. Ошибка #351'; return \json_encode($this->result); }
        if($this->winner != $this->pair->a && $this->winner != $this->pair->b) { $this->result->display = invalidDataFormat; return \json_encode($this->result); }
        $pair = $this->getPair();
        if(@$pair->handled || !$pair) { $this->result->display = 'Эта пара уже была обработана'; return \json_encode($this->result); }
        if(!$this->productsModel->getProductById($this->pair->a)) {
            $this->deletePair();
            $this->result->display = 'Эта пара уже была обработана'; return \json_encode($this->result);
        }
        $this->a = clone $this->productsModel->product;
        $this->a->categories = $this->productsModel->categories;
        if(!$this->productsModel->getProductById($this->pair->b)) {
            $this->deletePair();
            $this->result->display = 'Эта пара уже была обработана'; return \json_encode($this->result);
        }
        $this->b = clone $this->productsModel->product;
        $this->b->categories = $this->productsModel->categories;
        if($this->a->ptr && $this->b->ptr) {
            $this->deletePair();
            $this->result->display = 'Эти товары теперь нельзя сливать. Они оба принадлежат каталогу меркури';
            return \json_encode($this->result);
        }
        $this->winner = $this->winner == $this->a->product_id ? $this->a : $this->b;
        $this->looser = $this->winner->product_id == $this->a->product_id ? $this->b : $this->a;
        $this->mergeProducts();
        $this->result->result = 1;
        return \json_encode($this->result);
    }

    function mergeProducts() {
        if(is_object($this->looser->data)) {
            if(!is_object($this->winner->data)) {
                $this->idealProduct['data'] = \json_encode($this->looser->data);
            }
            if(is_object($this->winner->data) && is_object($this->looser->data)) {
                $this->idealProduct['data'] = \json_encode(@array_merge((array)$this->winner->data, (array)$this->looser->data));
            }
        }
        $this->idealProduct = [];
        $this->mergeParam('weight');
        $this->mergeParam('height');
        $this->mergeParam('weight');
        $this->mergeParam('length');
        $this->mergeParam('manufacturer_id');
        $this->mergeParam('short_description');
        $this->mergeParam('description');
        $this->mergeParam('description');
        $this->mergeParam('quantity');
        $this->mergeParam('meta_title');
        $this->mergeParam('meta_description');
        $this->mergeParam('meta_keywords');
        $this->mergeParam('old_alias');
        $this->mergeParam('old_link');
        $this->mergeParam('source_data');
        $this->mergeParam('ptr');
        $this->mergeParam('avatar');
        $this->mergeParam('title');
        $this->winner->images = \json_decode($this->winner->images);
        $this->looser->images = \json_decode($this->looser->images);
        $this->mergeCodes();
        $this->mergeCategories();
        $this->mergeConsistance();
        $this->crossMergeParam('images');

        $this->idealProduct['images'] = \json_encode($this->idealProduct['images']);
        $this->deletePair();

        $existingKeys = \DB::table('doubles')->where('a', $this->looser->product_id)->orWhere('b', $this->looser->product_id)->get();
        foreach($existingKeys as $k) {
            try {
                $updKey = $k->a == $this->looser->product_id ? 'a' : 'b';
                \DB::table('doubles')->where('a', $k->a)->where('b', $k->b)->where('code', $k->code)->update([$updKey => $this->winner->product_id]);
            } catch(\Exception $e) {
                \DB::table('doubles')->where('a', $k->a)->where('b', $k->b)->where('code', $k->code)->delete();
            }
        }
        \DB::table('doubles')->where('b', $this->winner->product_id)->where('handled', 0)->update(['b' => $this->winner->product_id]);
        \DB::table('products')->where('product_id', $this->looser->product_id)->delete();
        \DB::table('products')->where('product_id', $this->winner->product_id)->update($this->idealProduct);
        \DB::table('links')->where('id', $this->looser->link_id)->delete();
        \DB::statement("select update_product_data(".$this->winner->product_id.")");
    }

    function mergeCodes() {
        if($this->looser->codes) {
            foreach($this->looser->codes as $code) {
                try {
                    \DB::table('product_codes')->where('product_id', $this->looser->product_id)
                    ->where('product_code', $code)
                    ->update(['product_id', $this->winner->product_id]);
                } catch(\PDOException $e) {
                    \DB::table('product_codes')->where('product_id', $this->looser->product_id)
                        ->where('product_code', $code)
                        ->delete();
                }
            }
        }
    }

    function mergeCategories() {
        if($this->looser->categories) {
          foreach($this->looser->categories as $cat) {
            try {
                \DB::table('product_categories')->where('product_id', $this->looser->product_id)
                ->where('category_id', $cat->category_id)->update(['product_id' => $this->winner->product_id]);
            } catch(\Exception $e) {

            }
          }
        }
        \DB::table('product_categories')->where('product_id', $this->looser->product_id)
        ->delete();
    }

    function crossMergeParam($arg) {
        $this->idealProduct[$arg] = array_unique(array_merge((array)$this->winner->{$arg}, (array)$this->looser->{$arg}));
    }

    function mergeComplexes() {
        try {
        \DB::table('product_complex_consistance')->where('product_id', $this->looser->product_id)
        ->update(['product_id' => $this->winner->product_id]);
        } catch(\Exception $e) {

        }
        \DB::table('product_complex_consistance')->where('product_id', $this->looser->product_id)->delete();
    }


    function mergeConsistance() {
        try {
        \DB::table('product_consistance')->where('parent_product', $this->looser->product_id)
            ->update(['parent_product' => $this->winner->product_id]);
            \DB::table('product_consistance')->where('parent_product', $this->looser->product_id)->delete();
        } catch(\Exception $e) {

        }
        try {
        \DB::table('product_consistance')->where('child_product', $this->looser->product_id)
            ->update(['child_product' => $this->winner->product_id]);
            \DB::table('product_consistance')->where('child_product', $this->looser->product_id)->delete();
        } catch(\Exception $e) {

        }

    }

    function mergeParam($arg) {
        if($this->winner->{$arg} && is_string($this->winner->{$arg})) {
         if(!mb_strlen(trim($this->winner->{$arg}))) {
             $this->idealProduct[$arg] = $this->looser->{$arg};
             return;
         }
         return;
        }
        if($this->winner->{$arg}) { return; }
        $this->idealProduct[$arg] = $this->looser->{$arg};
    }

    function getPair() {
        return \DB::table('doubles')->where([
            'a' => $this->pair->a,
            'b' => $this->pair->b,
            'code' => $this->pair->code
        ])->first();
    }

    function getDoubles() {
        $user = \Auth::user();
        $ordering = @$user->username =='xqfce' ? 'asc' : 'desc';
        $this->page();
        $ids = \DB::table('doubles')->where('handled', 0)->orderBy('a', $ordering)->first();
        return $ids;
    }

    function deletePair() {
        if(!$this->pair) { $this->getRequestPair(); }
        \DB::table('doubles')->where('a', $this->pair->a)
        ->where('b', $this->pair->b)
        ->where('code', $this->pair->code)
        ->update(['handled' => 1]);
        return 1;
    }

    function getRequestPair() {
        $this->pair = (object)[
            'a' => \Request::get('a'),
            'b' => \Request::get('b'),
            'code' => \Request::get('code')
        ];
    }




}