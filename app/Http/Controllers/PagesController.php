<?php

namespace App\Http\Controllers;
use App\Models\CategoriesModel;

class PagesController extends FrontController {

    function mainPage() {
        $this->page();
        if(!\Cache::has('frontPage')) {
            $front = \json_decode(file_get_contents('../resources/data/index.json'));
            $cm = CategoriesModel::getInstance();
            $categories = [];
            foreach($front->categories as $k => $x) {
                $cat = $cm->getCategoryById($x);
                if($cat) { $categories[] = $cat; }
            }

            \Cache::forever('frontPage', \json_encode($categories));
        }
        $slides = $this->getSlidesData();
        return $this->view('pages.index', ['categories' => \json_decode(\Cache::get('frontPage')), 'slides' => $slides]);
    }

    function getSlidesData() {
        $slidesData = \DB::select("SELECT * FROM slides WHERE (publish_from >= current_date OR publish_from is null)
         AND (publish_to <= current_date OR publish_to is null) order by ordering asc");
        if(empty($slidesData)) { return $slidesData; }
        foreach($slidesData as $k => $v) {
            $slidesData[$k]->rules = \json_decode($slidesData[$k]->rules);
        }
        return $slidesData;
    }





}