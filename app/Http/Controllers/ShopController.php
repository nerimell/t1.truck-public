<?php


namespace App\Http\Controllers;
use App\Models\ShopModel;
use App\Models\UsersModel;
use App\Models\DeliveryModel;
use App\Models\PaymentsModel;


class ShopController extends FrontController {

    public $order;
    public $message;

    function __construct() {
        $this->model = ShopModel::getInstance();
    }

    function cartPage() {
        $usersModel = UsersModel::getInstance();
        $this->page((object)['meta_title' => 'Корзина']);
        $this->model->getCart();
        $userData = $usersModel->getUser(logged);
        $usersModel->completeRememberedUserData($userData);
        $arr = [
            'usertypes' => $usersModel->getUserTypes(),
            'blockCart' => 1,
            'require' => 1,
            'dialogCallback' => 'CartObject.cartCityCallback',
            'userFields' => $userData,
            'showPendingOrders' => 1,
            'coupon' => ShopModel::$coupon
        ];
        return $this->view('shop.cart', $arr);
    }

    function checkoutPage() {
        // проверить данные о товарах
        // проверить данные пользователя
        // страница оплаты
        // подтвердить заказ
        $this->page((object)['meta_title' => 'Оформление заказа']);
        return $this->view('shop.checkout', compact());
    }

    function validateCart() {
        $this->model->result = new \Result;
        $this->model->result->result = $this->model->validateCheckout();
        return \json_encode($this->model->result);
    }

    function addToCart() {
        return \json_encode($this->model->addProductToCart());
    }

    function setToCart() {
        return \json_encode($this->model->setProductToCart());
    }

    function setComplexToCart() {
        return \json_encode($this->model->setComplexToCart());
    }

    function setGifts() {
        return \json_encode($this->model->setGifts());
    }

    function addComplexToCart() {
        return \json_encode($this->model->addComplexToCart());
    }

    function deleteFromCart() {
        return \json_encode($this->model->deleteFromCart());
    }

    function activateCoupon() {
        return \json_encode($this->model->activateCoupon());
    }

    function forgetCoupon() {
        \Cookie::queue(\Cookie::forget('coupon'));
    }

    function calculateData() {
        return \json_encode($this->model->calculateData());
    }

    function setCartProductQty() {
        return \json_encode($this->model->setCartProductQty());
    }

    function buyInClick() {
        return \json_encode($this->model->buyInClick());
    }

    function postExternalProduct() {
        return \json_encode($this->model->postExternalProduct());
    }

    function calculateCartDelivery() {
        return \json_encode($this->model->calculateDeliveryCosts());
    }

    function calculateProductDelivery() {
        $dm = DeliveryModel::getInstance();
        return \json_encode($dm->calculateProductDeliveries());
    }

    function refreshCart() {
        $result = new \Result;
        $result->cart = $this->model->getCart();
        $result->inner = view('elements.shop.cart-inner', ['cart' => $result->cart])->render();
        return \json_encode($result);
    }

    function validateOrder() {
        $result = $this->model->validateOrder();
        return \json_encode($result);
    }


    function myOrder($hash) {
        $order = $this->model->getOrderByHash($hash);
        if(!$order) {
            abort(404);
        }
        if($order->user_id != logged) {
            $this->page((object)['meta_title' => 'Страница заказа']);
            return $this->view('shop.orders.restricted');
        }
        $allowChangeOrder = ShopModel::allowChangeOrder($order->hash);
        if(!$allowChangeOrder) {
            $this->page((object)['meta_title' => 'Страница заказа']);
            return $this->view('shop.orders.protected');
        }
        $this->page((object)['meta_title' => 'Заказ #'.$order->order_id]);
        $orderStatuses = $this->model->getOrderStatuses();
        $order->city_to = getCityById($order->city_to);
        $order->completeinfo = \json_decode($order->completeinfo);
        $order->userinfo = \json_decode($order->userinfo);
        $order->instances = \json_decode($order->instances);
        $this->model->reformatInstancesAvatars($order->completeinfo);
        return $this->view('shop.orders.order', compact('order', 'orderStatuses', 'allowChangeOrder'));
    }

    function formedOrder() {
        $hash = \Request::get('order_hash');
        $this->order = $this->model->getOrderByHash($hash);
        if(!$this->order) { abort(404); }
        $this->model->completeOrderData($this->order);
        $this->page((object)['meta_title' => 'Спасибо за заказ!']);
        $this->message = view('shop.orders.success', ['order' => $this->order]);
        return $this->hashOrderPage($hash);
    }

    function hashOrderPage($hash) {
        if(!$this->order) {
            $this->order = $this->model->getOrderByHash($hash);
            $this->model->completeOrderData($this->order);
        }
        if(!$this->order) { abort(404); }
        $this->page((object)['meta_title' => 'Заказ #'.$this->order->order_id]);

        $paymentsModel = new PaymentsModel;
        $paymentsModel->hash = $this->order->hash;
        $orderPaymentUrl = $paymentsModel->getPaymentUrl();
        $arr = [
            'order' => $this->order,
            'orderStatuses' =>  $this->model->getOrderStatuses(),
            'userData' => $this->order->userinfo,
            'redirectPayment' => \Request::isMethod('post'),
            'userFields' => $this->order->userinfo,
            'orderPaymentUrl' => $orderPaymentUrl->link,
            'message' => $this->message
        ];
        return $this->view('shop.orders.order', $arr);
    }

    function changeOrder($hash) {
        $this->page((object)['meta_title' => 'Редактирование заказа']);
        $order = $this->model->getOrderByHash($hash);
        if(!$order) { abort(404); }
        $this->page((object)['meta_title' => 'Заказ #'.$order->order_id]);
        $orderStatuses = $this->model->getOrderStatuses();
        $this->model->completeOrderData($order);
        $this->model->postCookieCart = $this->model->formatInstancesToCookieCart($order->instances);
        if(!$this->model->postCookieCart) {
            die(1); // нет данных о заказе или заказ не валиден
        }
        ShopModel::$coupon = $order->coupon;
        $usersModel = UsersModel::getInstance();
        $this->model->getCart();
        return $this->view('shop.cart', [
            'order' => $order,
            'orderStatuses' => $orderStatuses,
            'usertypes' => $usersModel->getUserTypes(),
            'require' => 1,
            'blockCart' => 1,
            'userFields' => $this->model->extractUserFields($order),
            'dialogCallback' => 'CartObject.cartCityCallback',
            'coupon' => $order->coupon
        ]);
    }

    function getContractByHash($hash) {
        $order = $this->model->getOrderByHash($hash);
        if(!$order) { abort(404); }
        $this->model->completeOrderData($order);
        return response($this->model->getPdfContract($order), 200)->header('Content-Type', 'application/pdf');
    }


}
