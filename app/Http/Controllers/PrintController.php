<?php

namespace App\Http\Controllers;

class PrintController extends FrontController {


    function printImage() {
        $link = \Request::get('link');
        $bot = curl_init();
        curl_setopt($bot, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($bot, CURLOPT_URL, $link);
        if(proxy) { curl_setopt($bot, CURLOPT_PROXY, proxy); }
        $response = curl_exec($bot);
        if(curl_getinfo($bot, CURLINFO_HTTP_CODE) != 200) { die('Неизвестная ошибка'); }
        return response($response, 200)->header('Content-Type', 'image/jpeg');
    }
}