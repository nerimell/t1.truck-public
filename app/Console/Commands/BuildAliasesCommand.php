<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;


class BuildAliasesCommand extends Command
{

    protected $signature = 'BuildAliasesCommand';
    protected $description;
    public $attempts = 0;
    public $product;
    public $title;


    public function __construct() {
        parent::__construct();
    }


    public function handle() {
        $product = \DB::table('products')
        ->select('product_id', 'title')->orderBy('product_id', 'asc')
        ->whereNull('alias')->first();
        $this->attempts = 0;
        $codes = \DB::table('product_codes')->where('product_id', $product->product_id)->get();
        $this->product = $product;
        $this->title =  $this->product->title;
        if($codes->count()) {
            $codesArr = [];
            foreach($codes as $code) {
                $codesArr[] = $code->product_code;
            }
            $this->title .=   ' ' . join(' ', $codesArr);
        }
        $this->toTable();
        return $this->handle();
    }

    function toTable() {
        try {
            $alias = \Str::slug($this->title);
            $alias = $this->attempts ? $alias.'-'.$this->attempts : $alias;
            \DB::table('products')->where('product_id', $this->product->product_id)
                ->update(['alias' => $alias]);
        } catch(\Exception $e) {
            $this->attempts++;
            return $this->toTable();
        }
    }
}
