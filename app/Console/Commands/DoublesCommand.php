<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
use App\Http\Controllers\DevController;

class DoublesCommand extends Command
{

    protected $signature = 'DoublesCommand';
    protected $description;


    public function __construct() {
        parent::__construct();
    }


    public function handle() {
       $this->iterate();
    }

    function iterate() {
        $product = \DB::table('products')->whereNull('temp')->first();
        $codes = \DB::table('product_codes')->where('product_id', $product->product_id)->get();
        if(empty($codes)) {
           $this->markProduct($product);
            return $this->iterate();
        }
        foreach($codes as $code) {
            $alterProducts = \DB::table('product_codes')->whereRaw(\DB::raw("LOWER(product_code) = LOWER('".$code->product_code."')"))->where('product_id', '!=', $product->product_id)->get();
            if(!$alterProducts->count()) { continue; }
            foreach($alterProducts as $alter) {
                $a = $product->product_id > $alter->product_id ? $alter->product_id : $product->product_id;
                $b = $product->product_id < $alter->product_id ? $alter->product_id : $product->product_id;
                try {
                    \DB::table('doubles')->insert(['a' => $a, 'b' => $b, 'code' => $code->product_code]);
                } catch(\Exception $e) {

                }
            }
        }
        $this->markProduct($product);
        $this->iterate();
    }

    function markProduct($product) {
        \DB::table('products')->where('product_id', $product->product_id)->update(['temp' => 1]);
    }
}
