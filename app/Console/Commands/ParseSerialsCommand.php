<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
use App\Models\Schedules\EzPartsCatalogParser\EzPartsCatalogParser;

class ParseSerialsCommand extends Command
{

    protected $signature = 'ParseSerialsCommand';
    protected $description;


    public function __construct() {
        parent::__construct();
    }


    public function handle() {
        $this->getSerials();
    }


    function getSerials() {

        $parser = new EzPartsCatalogParser();
        $parser->getSerials();
    }
}



