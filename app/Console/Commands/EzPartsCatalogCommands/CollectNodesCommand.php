<?php

namespace App\Console\Commands\EzPartsCatalogCommands;


use Illuminate\Console\Command;
use App\Models\Schedules\EzPartsCatalogParser\EzPartsCatalogParser;

class CollectNodesCommand extends Command
{

    protected $signature = 'CollectNodesCommand';
    protected $description;


    public function __construct(){
        parent::__construct();
    }

    public function handle() {
        $this->parseNodes();
    }

    function parseNodes() {
        $cls = new EzPartsCatalogParser();
        $cls->collectNodes();
    }


}


