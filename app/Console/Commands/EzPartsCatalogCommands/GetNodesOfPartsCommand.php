<?php

namespace App\Console\Commands\EzPartsCatalogCommands;


use Illuminate\Console\Command;
use App\Models\Schedules\EzPartsCatalogParser\EzPartsCatalogParser;

class GetNodesOfPartsCommand extends Command
{

    protected $signature = 'GetNodesOfPartsCommand';
    protected $description;


    public function __construct(){
        parent::__construct();
    }

    public function handle() {
        $this->parseNodes();
    }

    function parseNodes() {
        $cls = new EzPartsCatalogParser();
        $cls->getNodesOfParts();
    }


}


