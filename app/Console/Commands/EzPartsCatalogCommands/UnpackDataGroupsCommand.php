<?php

namespace App\Console\Commands\EzPartsCatalogCommands;


use App\Models\Schedules\EzPartsCatalogParser\EzPartsCatalogParser;
use Illuminate\Console\Command;
use App\Http\Controllers\DevController;

class UnpackDataGroupsCommand extends Command
{

    protected $signature = 'UnpackDataGroupsCommand';
    protected $description;


    public function __construct()
    {
        parent::__construct();
    }


    public function handle(){
       $cls = new EzPartsCatalogParser();
       $products = \DB::table('outsource_ezpartscatalog_nodes')->where('node_type', 'schematic')->get();
       foreach($products as $k => $v) {
           $cls->handleDataGroupInfo($v);
       }

    }


}


