<?php

namespace App\Console\Commands\EzPartsCatalogCommands;


use Illuminate\Console\Command;
use App\Models\Schedules\EzPartsCatalogParser\EzPartsCatalogParser;

class ChangeNodesOrderingCommand extends Command {

    protected $signature = 'ChangeNodesOrderingCommand';
    protected $description;


    public function __construct(){
        parent::__construct();
    }

    public function handle() {
        $ep = new EzPartsCatalogParser();
        $ep->changeNodesOrdering();
    }


}