<?php

namespace App\Console\Commands\EzPartsCatalogCommands;


use Illuminate\Console\Command;
use App\Models\Schedules\EzPartsCatalogParser\EzPartsCatalogParser;

class DownloadNullAvatarsCommand extends Command
{

    protected $signature = 'DownloadNullAvatarsCommand';
    protected $description;
    public $attempts = 0;
    public $product;
    public $title;


    public function __construct() {
        parent::__construct();
    }


    public function handle() {
        $ep = new EzPartsCatalogParser();
        $schemas = \DB::table('products')
            ->select('products.avatar', 'products.ptr', 'outsource_ezpartscatalog_nodes.avatar_http_code', 'outsource_ezpartscatalog_nodes.avatar_downloaded',
                'outsource_ezpartscatalog_nodes.avatar_data', 'products.product_id')
            ->join('outsource_ezpartscatalog_nodes', 'products.ptr', '=', 'outsource_ezpartscatalog_nodes.ptr')
            ->orderBy('products.product_id', 'asc')
            ->where('products.scheme', 1)
            ->get();
        $ep->auth($ep->bot);
        foreach($schemas as $k => $node) {
            $ep->parseNodeResourceData($node);
            $ep->getSchematicImage($node);
            if(!$node->avatar_downloaded) {
                echo 'there is no node '.$node->ptr.' avatar '.PHP_EOL;
                echo 'reason is '.@$node->reason.PHP_EOL;
                continue;
            }
            $source = 'resources/dev/resource_avatars/'.$node->avatar_downloaded;
            $dest = '../storage/public/i/nwmotors/products/'.$node->avatar_downloaded;
            $dest2 = '../storage/public/i/nwmotors/products/resized/'.$node->avatar_downloaded;
            if(!is_file($source)) { echo 'there is no such file in ' . $source.PHP_EOL; }
            copy($source, $dest);
            copy($source, $dest2);
            echo 'product with id ' .$node->product_id. ' now has avatar'.PHP_EOL;
            \DB::table('products')->where('product_id', $node->product_id)->update(['avatar' => $node->avatar_downloaded]);
        }
    }
}




