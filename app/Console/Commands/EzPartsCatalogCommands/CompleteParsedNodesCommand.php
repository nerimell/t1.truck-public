<?php

namespace App\Console\Commands\EzPartsCatalogCommands;


use Illuminate\Console\Command;
use App\Models\Schedules\EzPartsCatalogParser\EzPartsCatalogParser;

class CompleteParsedNodesCommand extends Command {

    protected $signature = 'CompleteParsedNodesCommand';
    protected $description;


    public function __construct(){
        parent::__construct();
    }

    public function handle() {
        $this->ep = new EzPartsCatalogParser();
        $this->nodes = \DB::table('outsource_ezpartscatalog_nodes_parsed')->get();
        foreach($this->nodes as $k => $node) {
            \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $node->ptr)->update(['temp' => 1]);
        }
    }


}