<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
use App\Models\MailerModel;

class ExternalProductsCommand extends Command
{

    protected $signature = 'ExternalProductsCommand';
    protected $description;


    public function __construct() {
        parent::__construct();
    }


    public function handle() {
        $offers = \DB::table('external_offers')->where('status', 0)->get();
        if(!$offers) { return; }
        $mm = new MailerModel;
        foreach($offers as $x) {
            $mm->sendExternalProduct($x);
        }
    }
}
