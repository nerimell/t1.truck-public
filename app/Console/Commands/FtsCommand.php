<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;


class FtsCommand extends Command
{

    protected $signature = 'FtsCommand';
    protected $description;


    public function __construct() {
        parent::__construct();
    }


    public function handle() {
        $products = \DB::table('products')->select('product_id')->get();
        foreach($products as $product) {
            \DB::statement("select update_product_data('".$product->product_id."')");
        }
    }
}
