<?php

namespace App\Console;

use App\Console\Commands\VkontakteCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\VkontakteCommand::class,
        Commands\ExternalProductsCommand::class,
        Commands\FtsCommand::class,
        Commands\DoublesCommand::class,
        Commands\EzPartsCatalogCommands\UnpackSerialsCommand::class,
        Commands\ParseSerialsCommand::class,
        Commands\EzPartsCatalogCommands\ParseResourceDataCommand::class,
        Commands\EzPartsCatalogCommands\UnpackDataGroupsCommand::class,
        Commands\DevCommand::class,
        Commands\BuildAliasesCommand::class,
        Commands\SyncCommand::class,
        Commands\EzPartsCatalogCommands\BuildTreeInActualTableCommand::class,
        Commands\EzPartsCatalogCommands\BuildNewTreeCommand::class,
        Commands\EzPartsCatalogCommands\ParseNodesCommand::class,
        Commands\EzPartsCatalogCommands\GetNodesOfPartsCommand::class,
        Commands\EzPartsCatalogCommands\CollectNodesCommand::class,
        Commands\EzPartsCatalogCommands\DownloadNullAvatarsCommand::class,
        Commands\EzPartsCatalogCommands\ChangeNodesOrderingCommand::class,
        Commands\EzPartsCatalogCommands\CompleteParsedNodesCommand::class
    ];



    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */



    protected function schedule(Schedule $schedule){
        include_once(public_path().'/../resources/data/constants.php');
        $schedule->command('VkontakteCommand')->dailyAt('11:00');
        $schedule->command('VkontakteCommand')->dailyAt('15:00');
    }



    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
