<?php
use App\Models\ShopModel;

    function anInt($a) {
        return ((string)(int)$a == $a);
    }

    function isCyrrilic($text) {
        return preg_match('/[А-Яа-яЁё]/u', $text);
    }

    function handleCategoryLink(&$cat) {
        if(@$cat->linkHandled) { return; }
        $cat->link = '/'.trim($cat->link, '/');
        $cat->linkHandled = 1;
    }

    function handleProductLink(&$prod) {
        if(@$prod->linkHandled) { return; }
        $prod->link = '/'.trim($prod->link, '/').'-detail';
        $prod->linkHandled = 1;
    }

    function checkLongInt($a) {
        $check = array_map(null, str_split($a));
        foreach($check as $x) {
            if(!\anInt($x)) { return 0; }
        }
        return 1;
    }

    function formatTimezoneOffset($tz) {
        if(!anInt($tz)) { $tz = 0; }
        $tz = (int)($tz*-1);
        if($tz < -720) { $tz = -720; }
        if($tz > 720) { $tz = 720; }
        return $tz;
    }

    function validateEmail($a) {
        if(!filter_var($a, FILTER_VALIDATE_EMAIL)) { return 0; }
        return 1;
    }

    function validatePhone($a) {
        if(mb_strlen($a) != 15) { return 0; }
        if($a[0] != '(') { return 0; }
        if($a[4] != ')') { return 0; }
        if($a[5] != ' ') { return 0; }
        if($a[9] != '-') { return 0; }
        if($a[12] != '-') { return 0; }
        return 1;
    }

    function validateTime($a) {
        $hour = substr($a, 0, 2);
        $minute = substr($a, 2, 4);
        if($hour > 24 || $hour < 0) { return 0; }
        if($minute > 60 || $minute < 0) { return 0; }
        return 1;
    }

    function isRus($a) {
        return preg_match( '/[\p{Cyrillic}]/u', $a);
    }

    function timezone($tz) {
        $tz = (float)$tz;
        \Cookie::queue(\Cookie::make('tz', $tz, time()+2678400));
        return $tz;
    }

    function dd($string = false) {
        print_r($string); die;
    }

    function ddd($string = false) {
        if(!\Request::get('asd')) { return; }
        print_r($string); die;
    }

    function arrayMap($a, $k, $multi = 0) {
        if(empty($a)) { return 0; }
        $b = [];
        foreach($a as $aa) {
            if($multi) {
                if(!isset($b[$aa->$k])) { $b[$aa->$k] = []; }
                $b[$aa->$k][] = $aa;
            } else {
                $b[$aa->$k] = $aa;
            }
        }
        return $b;
    }


    function getUserCity() {
        $city = (int)\Request::get('city');
        $dbCity = 0;
        $city = $city ? $city : (\Cookie::has('city') ? (int)\Cookie::get('city') : 0);
        define('cd', $city ? 1 : 0); // cd = city defined
        if($city) { $dbCity = getCityById($city); }
        if(!$dbCity) { $dbCity = getCityById(defaultCity); }
        \Cookie::queue(\Cookie::make('city', $dbCity->city_id, time()+2678400));
        \Html::$city = $dbCity;
    }

    function getCityById($id) {
        return \DB::table('cities')
            ->select('cities.*', 'regions.title as region_title')
            ->join('regions', 'cities.region_id', '=', 'regions.region_id')
            ->where('cities.city_id',  $id)->first();
    }

    function toArray($arr) {
        if(empty($arr)) { return $arr; }
        if(!is_array($arr)) { $arr = (array)$arr; }
        $keys = count($arr);
        $final = [];
        for($i = 0; $i < $keys; $i++) {
            $final[] = array_shift($arr);
        }
        return $final;
    }

    function rusificate($num, $form_for_1, $form_for_2, $form_for_5){
        $num = abs($num) % 100;
        $num_x = $num % 10;
        if ($num > 10 && $num < 20) { return $form_for_5; }
        if ($num_x > 1 && $num_x < 5) { return $form_for_2; }
        if ($num_x == 1) { return $form_for_1; }
        return $form_for_5;
    }

    function prepareString($s, $max) {
        $length = mb_strlen($s);
        if($length > $max) { return mb_substr($s, 0, $max).'...'; }
        return $s;
    }

    function calculateComplexPrice(&$complex) {
        $complex->priceinfo = (object)[
            'price' => 0,
            'final_price' => 0,
            'base_discount' => 0,
            'discount_value' => 0,
            'discount_percent' => 0,
            'coupon_discount' => 0,
            'discount' => 0,
            'complex_discount_percent' => 0,
            'total_discount_percent' => 0
        ];
        for($i = 0; $i < $complex->productsCount; $i++) {
           // dd($complex->products[$i]);
            calculateProductPrice($complex->products[$i]);
            $complex->priceinfo->price += $complex->products[$i]->priceinfo->price;
            $complex->priceinfo->final_price += $complex->products[$i]->priceinfo->final_price;
            $complex->priceinfo->discount_value += $complex->products[$i]->priceinfo->price-$complex->products[$i]->priceinfo->final_price;
            $complex->priceinfo->base_discount += $complex->products[$i]->priceinfo->base_discount;
            $complex->priceinfo->discount_percent += $complex->products[$i]->priceinfo->price/100*$complex->products[$i]->discount;
            $complex->priceinfo->coupon_discount += $complex->products[$i]->priceinfo->coupon_discount;
            $complex->priceinfo->complex_discount_percent = $complex->products[$i]->discount;
        }
       $complex->priceinfo->total_discount_percent = $complex->priceinfo->price ? round(($complex->priceinfo->discount_value/$complex->priceinfo->price*100)) : 0;
       $complex->priceinfo->discount = $complex->priceinfo->total_discount_percent ? 1 : 0;
       $finalPrice = (int)($complex->priceinfo->final_price/100*(100-$complex->priceinfo->complex_discount_percent));
       $complex->priceinfo->base_discount += $complex->priceinfo->final_price-$finalPrice;
       $complex->priceinfo->final_price = $finalPrice;
    }

    function calculateProductPrice(&$product) {
        if(@$product->priceinfoHandled) { return; }
        $priceinfo = is_object(@$product->priceinfo) ? $product->priceinfo : \json_decode(@$product->priceinfo);
        $product->price = @$priceinfo->price ? (float)$priceinfo->price : 0;
        $product->discount_price = @$priceinfo->discount_price ? (float)$priceinfo->discount_price : 0;
        $pricesEquals = ($product->price && !$product->discount_price || ($product->discount_price && $product->discount_price != $product->price && $product->discount_price < $product->price)) ? 0 : 1;
        $result = [
            'price' => round($product->price)
        ];
        $dv = 0;
        if($product->price && @ShopModel::$coupon->discount_value) {
            eval('$dv = $product->price - ('.$product->price.' '.ShopModel::$coupon->regex.');');
        }
        $dv = (int)round($dv);
        if(!$pricesEquals) { // товар имеет свою скидку
            $result['coupon_discount'] = $dv;
            $result['discount'] = 1;
            $result['final_price'] = (int)round($product->discount_price -$result['coupon_discount']);
            $result['compare'] = round((float)$result['final_price'], 2);
            $result['discount_value'] = round(($product->price - ($product->discount_price-$result['coupon_discount'])));
            $result['base_discount'] = round($product->price - $product->discount_price);
            $result['discount_percent'] = $product->price ? round((($result['discount_value']/$result['price']*100))) : 0;
            $product->priceinfoHandled = 1;
            $product->priceinfo = (object)$result;
            return;
        }



        $result['discount'] = $dv ? 1 : 0;
        $result['discount_value'] = $result['price'] - ($result['price'] - $dv);
        $result['coupon_discount'] = $dv;
        $result['discount_percent'] = $dv ? (round((($dv/$result['price']*100)))) : 0;
        $result['base_discount'] = 0;
        $result['final_price'] = round($dv ? $product->price-$dv : $product->price);
        $result['compare'] = round((int)$result['final_price'], 2);
        $product->priceinfoHandled = 1;
        $product->priceinfo = (object)$result;
        return;

    }