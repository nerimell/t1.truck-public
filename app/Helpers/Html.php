<?php

namespace App\Helpers;
use Carbon\Carbon;

class Html {



    public $user;
    public static $now;
    public static $city;
    public static $primaryKey;
    private static $_instance = null;

    private function __construct() {
        $this->user = \Auth::user();
    }



    static function img($path, $attributes = '') {
        return '';
    }

    static function picture($path, $attributes = '') {
        if(!$path) { return '<img src="'.defaultImage.'" '.$attributes.' />'; }
        return '<img src="'.storageUrl.$path.'" onerror=\'this.src="'.defaultImage.'"; this.classList.add("not-found-image")\' '.$attributes.' />';
    }

    static function productPicture($path, $attributes = '') {
        if(!$path) { return '<img src="'.defaultImage.'" class="not-found-image" '.$attributes.' />'; }
        return '<img src="'.self::productPicturePath($path).'" onerror=\'this.src="'.defaultImage.'"; this.classList.add("not-found-image") \' '.$attributes.' />';
    }

    static function productPicturePath($path) {
        return storageUrl.'products/'.$path;
    }

    static function productThumbnail($path, $attributes = '') {
        if(!$path) { return '<img src="'.defaultImage.'" '.$attributes.' class="not-found-image" />'; }
        return '<img src="'.self::productThumbnailPath($path).'" onerror=\'this.src="'.defaultImage.'"; this.classList.add("not-found-image")\' '.$attributes.' />';
    }



    static function productThumbnailPath($path) {
        $basename = basename($path);
        $url = storageUrl.'products/resized/'.$basename;
        return $url;
    }

    static function productBarcode($path, $attributes = '') {
        if(!$path) { return '<img src="'.defaultImage.'" '.$attributes.' />'; }
        return '<img src="'.self::productBarcodePath($path).'" onerror="Actions.product.removeBarcodeRow(this)" onload="Actions.product.addBarcodeRow(this)" '.$attributes.' />';
    }

    static function productBarcodePath($path) {
        return storageUrl.'barcodes/'.$path;
    }


    static function masterPicture($path, $attributes ='') {
        return '<img src="'.storageUrl.'images/'.$path.'" onerror=\'this.src="'.defaultImage.'"\' '.$attributes.' />';
    }

    static function systemPicture($path, $attributes ='') {
        return '<img src="/images/'.$path.'" onerror=\'this.src="'.defaultImage.'"\' '.$attributes.' />';
    }


    static function masterPicturePath($path) {
        return storageUrl.'images/'.$path;
    }

    static function calculateProductPrice($product) {
        $priceinfo = \json_decode(@$product->priceinfo);
        $product->price = @$priceinfo->{priceStorage}->price ? (float)$priceinfo->{priceStorage}->price : 0;
        $product->discount_price = @$priceinfo->{priceStorage}->discount_price ? (float)$priceinfo->{priceStorage}->discount_price : 0;
        $pricesEquals = ($product->price && !$product->discount_price || ($product->discount_price && $product->discount_price != $product->price && $product->discount_price < $product->price)) ? 0 : 1;
        $result = [
            'price' => round($product->price),
        ];
        if(!$pricesEquals && !override_discount) { // товар имеет свою скидку и глобальная скидка не переписывает скидки товаров
            $result['final_price'] = round($product->discount_price);
            $result['discount'] = 1;
            $result['discount_value'] = round(($product->price - $product->discount_price));
            $result['discount_percent'] = $product->price ? round(($result['discount_value']/$result['price']*100)) : 0;
            $result['compare'] = round((float)$result['final_price'], 2);
            return $result;
        }
        if(discount_value) {
            $result['discount'] = 1;
            $result['discount_value'] = $product->price/100*(100-discount_value);
            $result['discount_percent'] = $product->price ? round(100-($result['discount_value']/$result['price']*100)) : 0;
            $result['final_price'] = $product->discount_price;
            $result['compare'] = round((float)$result['final_price'], 2);
            return $result;
        }

        $result['final_price'] = $pricesEquals ? $product->price : round($product->discount_price);
        $result['discount'] = $pricesEquals ? 0 : 1;
        $result['discount_value'] = $pricesEquals ? 0 : round(($product->price - $product->discount_price));
        $result['discount_percent'] = $pricesEquals ? 0 : (round(($result['discount_value']/$result['price']*100)));
        $result['compare'] = round((float)$result['final_price'], 2);
        return $result;

    }

    static function handleProducts(&$products, &$priceRowsLg, &$priceRowsMd, &$codeRowsLg, &$codeRowsMd, &$basePriceRowsLg, &$basePriceRowsMd) {
        $i = 1;
        $rowLg = 1;
        $rowMd = 1;
        $total = count($products);
        for($k = 0; $k < $total; $k++) {
            calculateProductPrice($products[$k]);
            $products[$k]->rowLg = $rowLg;
            $products[$k]->rowMd = $rowMd;
            $products[$k]->handled = 1;
            if(!empty($products[$k]->codes)) { $codeRowsMd[$rowMd] = 1; $codeRowsLg[$rowLg] = 1; }
            if($products[$k]->priceinfo->compare && @$products[$k]->sells) {
                $priceRowsMd[$rowMd] = 1; $priceRowsLg[$rowLg] = 1;
                if($products[$k]->priceinfo->discount_value) {
                    $basePriceRowsLg[$rowLg] = 1;
                    $basePriceRowsMd[$rowMd] = 1;
                }
            }
            if($i % 3 == 0) { $rowLg++; }
            if($i % 2 == 0) { $rowMd++; }
            $i++;
        }
    }



    static function getInstance() {
        if(is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    static function getImage($d) {
        if($d) {
            $path = trim('uploads/'.$d, '/');
            if(!is_file($path)) {
                return defaultImage;
            }
            return '/'.$path;
        }
        return defaultImage;
    }


    static function getAvatar($id) {
        return storageUrl.'i/'.$id.'/avatar/avatar.png';
    }



}
