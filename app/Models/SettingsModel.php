<?php
namespace App\Models;

class SettingsModel {

    public $website;
    public $pageInfo = [];
    public $siteData;
    public $offices;
    public $officesByIds;
    public static $_instance = 0;
    public $defined = 0;

    private function __construct() { }

    public static function getInstance() {
        if(self::$_instance) { return self::$_instance; }
        return self::$_instance = new self;
    }
    function getSiteData() {
        if($this->siteData) { return $this->siteData; }
        return $this->siteData = \json_decode(file_get_contents(public_path().'/../resources/data/settings.json'));
    }

    function define() {
        if($this->defined) { return; }
        $this->getSiteData();
        define('freeDeliveryLimit', $this->getSiteData()->freeDeliveryLimit);
    }

    function shareSiteData($pageInfo = false) {
        if($pageInfo) { $this->pageInfo = $pageInfo; }
        $this->website = $this->getSiteData();
        $this->website = $this->mergeSiteData($this->website, $this->pageInfo);
        $user = \Auth::user();
        if($user) {
            define('protectedView', '-logged');
        } else {
            define('protectedView', '');
        }
        $this->getOfficesData();
        $footer = json_decode(file_get_contents('../resources/data/footer.json'));
        $this->define();
        \View::share(['website' => $this->website, 'user' => $user, 'footer' => $footer,
        'offices' => $this->offices,
        'officesByIds' => $this->officesByIds]);
    }

    function mergeSiteData($a, $b) {
        $a->meta_title = (!empty($b->meta_title)) ? strip_tags($b->meta_title).' - '.@$a->title : @$a->title;
        $a->meta_description = (!empty($b->meta_description)) ? strip_tags($b->meta_description) : $a->meta_description;
        $a->meta_keywords = (!empty($b->meta_keywords)) ? strip_tags($b->meta_keywords) : $a->meta_keywords;
        return $a;
    }

    function getOfficesData() {
        if($this->offices) { return $this->offices; }
        $this->offices = \DB::table('cities')->whereNotNull('office')->get();
        if(empty($this->offices))  { return; }
        $this->officesByIds = [];
        foreach($this->offices as $k => $v) {
            $this->offices[$k]->office = \json_decode($v->office);
            $this->officesByIds[$v->city_id] = $this->offices[$k];
        }
        return $this->offices;
    }

}