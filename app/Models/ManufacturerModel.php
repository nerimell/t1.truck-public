<?php

namespace App\Models;
use App\Models\Model;
class ManufacturerModel extends Model {

    public $manufacturer;
    public $products;
    public $total;

    function getManufacturerByAlias($alias) {
        $this->manufacturer = \DB::table('manufacturers')->where('alias', $alias)->first();
        if(!$this->manufacturer) { return 0; }
        $this->products = $this->getManufacturerProducts($this->manufacturer->manufacturer_id);
        return $this;
    }

    function getManufacturerProducts($id) {
        return 1;
    }

    static function getSimpleManufacturer($id) {
        if(!$id) { return 0; }
        return \DB::table('manufacturers')
          ->where('manufacturers.manufacturer_id', $id)
          ->select('manufacturers.alias', 'manufacturers.title', 'manufacturers.guarantee_text')
          ->first();
    }

    function collectParams() {
        $this->params = [];
        $this->params['p'] = (int)\Request::get('page');
        $this->params['p'] = $this->params['p'] < 1 ? 1 : $this->params['p'];
    }

}
