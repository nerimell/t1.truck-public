<?php

namespace App\Models;
use YandexCheckout\Client;

class PaymentsModel {

    public $paymentMethods;
    public $order;
    public $settings;
    public $hash;
    public $client;
    public $inited = 0;

    function init() {
        if($this->inited) { return; }
        $this->client = new Client();
        $this->getSettings();
        $this->client->setAuth($this->settings->yandexMoney->shopId, $this->settings->yandexMoney->secret);
        $this->inited = 1;
    }

    function getPaymentMethods() {
        if($this->paymentMethods) { return $this->paymentMethods; }
        return $this->paymentMethods = [

        ];
    }

    function getPaymentById($id) {
        return \DB::table('payments')->where('payment_id', $id)->first();
    }

    function getTmpOrderById() {

    }

    function confirmPayment($requestData) {
        $paymentData = $requestData['object'];
        $payment = $this->getPaymentById($paymentData['id']);
        file_put_contents('..resources/dev/asdd.txt', 1);
        if(!$payment) {
            file_put_contents('../resources/dev/2.json', 'fail step 1');
            $this->cancelPayment($requestData);
            return 0;
        }
        $this->order = @\DB::table('tmp_orders')->where('tmp_order_id', $payment->tmp_order_id)->first();
        if(!$this->order) {
            file_put_contents('../resources/dev/2.json', 'fail step 2');
            $this->cancelPayment($requestData);
            return 0;
        }
        $sm = ShopModel::getInstance();
        ShopModel::$currentOrderId = $payment->tmp_order_id;
        $sm->cart = null;
        $sm->getCart();
        $this->cart->totals->final_price = 1;
        file_put_contents('../resources/dev/5.json', 'step 4');
        if($sm->cart->totals->final_price.'.00' != $paymentData['amount']['value']) {
            file_put_contents('../resources/dev/2.json', $sm->cart->totals->final_price. '.00 != '.$paymentData['amount']['value']);
            $this->cancelPayment($requestData);
            return 0;
        }
        file_put_contents('../resources/dev/3.json', 'step 4');
        $this->client->capturePayment([
                'amount' => $requestData['object']['amount']['value'],
            ],
            $payment->payment_id,
            uniqid($this->getUniqued(), true)
        );
    }

    function sendOrder($order) {
        $this->order = $order;
        return $this->redirectToYandexMoney();
    }

    function getPaymentUrl() {
        $this->result = new \Result;
        $this->result->hash = $this->hash ? $this->hash : \Request::get('hash');
        if(!$this->result->hash) { $this->result->display = invalidDataFormat; return $this->result; }
        $sm = ShopModel::getInstance();
        $this->result->order = $sm->getOrderByHash($this->result->hash);
        if(!$this->result->order) { $this->result->display = 'Такого заказа не существует. Попробуйте обновить страницу'; return $this->result; }
        $this->order = &$this->result->order;
        $this->result->link = $this->makePaymentLink();
        $this->result->result = 1;
        return $this->result;
    }

    function makePaymentLink() {
        return $this->makeYandexPaymentLink();
    }

    function getSettings() {
        $this->settings = (SettingsModel::getInstance())->getSiteData();
    }

    function cancelPayment($paymentData) {
        $this->init();
        $this->client->paymentId = $paymentData['object']['id'];
        $this->client->capturePayment([
                'amount' => $paymentData['object']['amount']['value'],
            ],
            $paymentData['object']['id'],
            uniqid($this->getUniqued(), true)
        );
    }

    function getUniqued() {
        return md5('asd');
    }

    function makeYandexPaymentLink() {
        $this->init();
        $this->order->price = 1;
        $payment = $this->client->createPayment([
            'amount' => ['value' => (int)$this->order->price.'.00', 'currency' => 'RUB'],
            'confirmation' => ['type' => 'redirect', 'return_url' => env('APP_URL').'/thanks'],
        ], uniqid($this->getUniqued(), true));
        \DB::table('payments')->insert(['payment_id' => $payment->id, 'tmp_order_id' => $this->order->tmp_order_id]);
        return $payment->confirmation->confirmation_url;

    }

}