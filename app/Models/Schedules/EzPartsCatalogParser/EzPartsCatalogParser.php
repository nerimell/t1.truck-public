<?php

namespace App\Models\Schedules\EzPartsCatalogParser;

class EzPartsCatalogParser {

    public $bot;
    private $bots = [];
    private $threadBot;
    public $defaultHeaders;
    private $agents;
    private $cookie;
    public $lastHeaders;
    public $errors = [];
    public $productErrors = [];
    public $noProductResources = [];
    public $data = [];
    public $lastProxy;
    public $ignoreExists = 1;
    public $proxy = 1;
    public $nodes;
    public $cookiepath;
    public $cookiesTmp;
    public $rootNode = '';
    public $limit = 664000;
    public $proxies = [
        '118.97.87.186:8080'
    ];
    public $nodeType;
    public $scannableNodeTypes = ['openfolder', 'model'];


    function __construct() {
        $this->cookiepath = public_path().'/../resources/schedules/cookies/EzPartsParser.txt';
        $this->cookiesTmp = public_path().'/../resources/schedules/cookies/tmp/ezPartsParser/';
        $this->proxy = 1;
        $this->init();
        $this->initBot($this->bot);
    }

    function schemesToTables() {
        \DB::table('products')->where('scheme', 1)->delete();
        $this->nodes = \DB::table('outsource_ezpartscatalog_nodes')->where('node_type', 'schematic')->get();
        \Html::$primaryKey = 'product_id';
        foreach($this->nodes as $node) {
            echo 'creating node '.$node->ptr.PHP_EOL;
            $this->parentCat = \DB::table('categories')->where('ptr', $node->parent_ptr)->first();
            if(!$this->parentCat) { echo 'node ' .$node->ptr . ' has no parent'; continue; }
            $this->attempts = 0;
            $this->createScheme($node);
        }
    }

    function createScheme(&$node) {
        $exists = \DB::table('products')->where('ptr', $node->ptr)->first();
        if($exists) { return; }
        $alias = \Str::slug($node->title);
        if($this->attempts) {
            if($this->attempts == 1) {
                $alias = \Str::slug($this->parentCat->title).'-'.$alias;
            } else {
                $alias = \Str::slug($this->parentCat->title).'-'.$alias.'-'.$this->attempts;
            }
        }
        try {
        $productId = \DB::table('products')->insertGetId([
            'title' => $node->title,
            'ordering' => $node->ordering,
            'avatar' => $node->avatar_downloaded,
            'scheme' => 1,
            'sells' => 0,
            'template' => 'ez-parts-catalog-scheme',
            'alias' => $alias,
            'ptr' => $node->ptr
        ]);
        } catch(\Exception $e) {
            $this->attempts++;
            return $this->createScheme($node);
        }
        try {
            \DB::table('product_categories')->insert([
                'product_id' => $productId,
                'category_id' => $this->parentCat->category_id
            ]);
        } catch(\Exception $e) {
            echo 'WARN'.PHP_EOL;
        }

    }

    function changeNodesOrdering() {
        $this->nodes = \DB::table('outsource_ezpartscatalog_nodes_parsed')->get();
        $this->nodesByPtr = [];
        foreach($this->nodes as $node) {
            $this->nodesByPtr[$node->ptr] = $node;
        }
        foreach($this->nodes as $node) {
            if($node->is_final) { continue; }
            $childs = \json_decode($node->childs);
            $i = 1;
            foreach($childs as $child) {
                $this->nodesByPtr[$child[0]]->ordering = $i;
                $i++;
            }
        }
        foreach($this->nodesByPtr as $node) {
            \DB::table('outsource_ezpartscatalog_nodes_parsed')->where('ptr', $node->ptr)->update(['ordering' => $node->ordering]);
        }

    }

    function parseNodes() {
        $this->getNodesForParsing();
        $this->auth($this->bot);
        foreach($this->nodes as $node) {
            $this->parseNodeByPtr($node);
        }
    }

    function parseNodeByPtr($node) {
        $this->parseNodeResourceData($node);
        $this->parseNodeDataGroups($node);
        $this->getSchematicImage($node);
       // $this->handleDataGroupInfo($node);
        \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $node->ptr)->update(['synced_at' => date('Y-m-d H:i:s')]);
    }

    function getNodesForParsing() {
        $this->nodes = \DB::table('products')
            ->join('outsource_ezpartscatalog_nodes', 'outsource_ezpartscatalog_nodes.ptr', '=', 'products.ptr')
            ->whereNull('scanned_at')
            ->where('products.ptr', '24554332234746')
            ->limit($this->limit)
            ->get();
    }

    function auth(&$bot) {
        curl_setopt($bot, CURLOPT_URL,
  'https://public-mercurymarine.sysonline.com/Default.aspx?sysname=NorthAmerica&company=Guest&NA_KEY=NA_KEY_VALUE&langIF=eng&langDB=eng');
        curl_setopt($bot, CURLOPT_HTTPHEADER, [
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding: gzip, deflate, sdch, br',
            'Accept-Language: ru,en-US;q=0.8,en;q=0.6',
            'Connection: keep-alive',
            'Host: public-mercurymarine.sysonline.com',
            'Referer: https://public-mercurymarine.sysonline.com/Default.aspx?sysname=NorthAmerica&company=Guest&NA_KEY=NA_KEY_VALUE&langIF=eng&langDB=eng',
            'Upgrade-Insecure-Requests: 1',
            'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36'
        ]);
        curl_setopt($bot, CURLOPT_HTTPGET, 1);
        curl_setopt($bot, CURLOPT_ENCODING , "gzip");
        $auth = curl_exec($bot);

    }

    function init() {
        $this->bot = curl_init();
        $this->agents = (array)\simplexml_load_file(public_path().'/../resources/schedules/agents.xml');
        $this->cookie = realpath($this->cookiepath);
    }

    function initBot(&$bot) {

        curl_setopt($bot, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($bot, CURLOPT_FOLLOWLOCATION, 1);
        $this->setRandomCookie($bot);
        curl_setopt($bot ,CURLOPT_TIMEOUT, 100000);
        curl_setopt($bot, CURLOPT_TIMEOUT, 20);
        curl_setopt($bot, CURLOPT_CONNECTTIMEOUT ,0);
        $this->getProxy($bot);
    }

    function getSerials() {

        $serials = \DB::table('outsource_ezpartscatalog_serials')
        ->orderBy('id', 'desc')
        ->take(1000)->skip(rand(0, 750000))->whereNull('response_status')->get();
        $this->init();
        $this->initBot($this->bot);
        $this->auth($this->bot);
        foreach($serials as $k => $v) {
            $this->getSerialNodes($v);
        }
    }



    function getSerialNodes($v) {
        $v->serial .='\r';
        $v->serial = trim(str_replace('\r', '', $v->serial));
        $post = '{"serialGroupPtr":"25005299597313","serialNumber":"'.$v->serial.'","serialGroupName":"All serials"}';
        $opts = ['X-AjaxPro-Method' => 'getSerialResult', 'Accept' => '*/*'];
        curl_setopt($this->bot, CURLOPT_POST, 1);
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxSearchControl,App_Web_rxddsmh4.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
        $decoded = \json_decode($response);
        echo 'serial '.$v->serial. ' has been parsed'.PHP_EOL;
        \DB::table('outsource_ezpartscatalog_serials')
            ->where('id', $v->id)
            ->update(['serial' => $v->serial, 'response' => $response, 'response_status' => @$decoded->value->Serials ? 1 : 0]);
    }

    function setHeaders($opts = false, $bot) {
        $this->defaultHeaders = [
            'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding' => 'gzip, deflate, sdch, br',
            'Accept-Language' => 'en,en-GB;q=0.8,en;q=0.6',
            'Connection' => 'keep-alive',
            'Content-Type' => 'text/plain; charset=UTF-8',
            'Host' => 'public-mercurymarine.sysonline.com',
            'Origin' => 'https://public-mercurymarine.sysonline.com',
            'Referer' => 'https://public-mercurymarine.sysonline.com/Default.aspx?sysname=NorthAmericacompany=GuestNA_KEY=NA_KEY_VALUElangIF=englangDB=eng',
            'User-Agent' => $this->getAgent(),
        ];
        $this->lastHeaders = $opts ? $this->mergeHeaders($opts) : $this->defaultHeaders;
        $headers = [];
        foreach($this->lastHeaders as $k => $v) {
            $headers[] = $k.': '.$v;
        }
        curl_setopt($bot, CURLOPT_HTTPHEADER, $headers);
    }

    function mergeHeaders($opts) {
        $headers = $this->defaultHeaders;
        foreach($opts as $k => $v) {
            $headers[$k] = $v;
        }
        return $headers;

    }

    function getAgent() {
        return $this->agents[array_rand($this->agents, 1)][0];
    }

    function getProxy(&$bot) {
        if(!$this->proxy) { return; }
        $this->lastProxy = $this->proxies[array_rand($this->proxies)];
        curl_setopt($bot, CURLOPT_PROXY, 'https://'.$this->lastProxy.'/');
    }

    function request($opts, &$bot) {
        $this->setHeaders($opts, $bot);
        $this->getProxy($bot);
        $response = curl_exec($bot);
        $status = curl_getinfo($bot, CURLINFO_HTTP_CODE);
        if($status == 200) {
            return $response;
        }
        return false;
    }

    function getPartByCode($code, $bot) {
        $post = json_encode(['searchCondition' => [$code, 'PartsByCode', null, null, false]]);
        $opts = ['X-AjaxPro-Method' => 'getSearchResult', 'Accept' => '*/*'];
        curl_setopt($bot, CURLOPT_POST, 1);
        curl_setopt($bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxSearchControl,App_Web_r5ymwjnn.ashx');
        curl_setopt($bot ,CURLOPT_ENCODING , "gzip");
        $response = \json_decode($this->request($opts, $bot));
        if(!$response || !@count($response->value->PartsByCode)) { $this->productErrors[$code] = 1; return false; }
        $product = 0;
        foreach($response->value->PartsByCode as $x) {
            if($x[1][0] != $code) { continue; }
            $product = new \stdClass;
            $product->ptr = $x[0];
            $product->code = $x[1][0];
            $product->title = $x[1][1];
        }
        if(!$product) { $this->productErrors[$code] = 1; return false; }
        return $product;
    }

    function setOptPartQuery($code, $bot) {
        $post = json_encode(['searchCondition' => [$code, 'PartsByCode', null, null, false]]);
        $opts = ['X-AjaxPro-Method' => 'getSearchResult', 'Accept' => '*/*'];
        curl_setopt($bot, CURLOPT_POST, 1);
        curl_setopt($bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxSearchControl,App_Web_r5ymwjnn.ashx');
        curl_setopt($bot ,CURLOPT_ENCODING , "gzip");
        $this->setHeaders($opts, $bot);
    }

    function getNodesOfPart($partId) {
        $post = json_encode(['partPtr' => $partId, "curModelPtr" => null]);
        $opts = ['X-AjaxPro-Method' => 'getPartUsingInResources', 'Accept' => '*/*'];
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxSearchControl,App_Web_rxddsmh4.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
        $response = \json_decode($response);
        if(!$response || !@count(@$response->value->DataCache)) { $this->noProductResources[$partId] = 1; return false; }
        return $response;
    }

    function collectNodes() {
        $this->nodes = $this->getRootNodes();
        $this->auth($this->bot);
        foreach($this->nodes as $node) {
            $this->recursiveGetNodesOfNode($node);
        }
        echo 'nodes collected'.PHP_EOL;
    }

    function getParsedNodes() {
        return \DB::table('outsource_ezpartscatalog_nodes')->get();
    }

    function buildTreeInActualTables() {
        $this->nodes = \DB::table('outsource_ezpartscatalog_nodes')->get();
        $this->buildActualCategories();
    }

    function createCategoryFromNode($v) {
        $alias = \Str::slug($v->title);
        $alias = $this->attempts ? $alias.'-'.$this->attempts : $alias;
        try {
            $id = \DB::table('categories')->insertGetId(['ptr' => $v->ptr,
             'title' => $v->title,
             'temp' => 1,
             'alias' => $alias,
             'ordering' => $v->ordering,
             'status' => $this->status
            ]);
            return $id;
        } catch(\PDOException $e) {
            $this->attempts++;
            return $this->createCategoryFromNode($v);
        }
    }

    function buildActualCategories() {

        $i = 0;
        $this->nodesByPtr = [];
        foreach($this->nodes as $node) {
            $this->nodesByPtr[$node->ptr] = $node;
        }
        \Html::$primaryKey = 'category_id';
        foreach($this->nodes as $k => $v) {
            if($v->node_type != 'openfolder' && $v->node_type != 'model') { continue; }
            $this->status = $v->childs == '[]' ? false : true;
            $existingCat = \DB::table('categories')->where('ptr', $v->ptr)->first();
            if($existingCat) {
                $this->nodesByPtr[$v->ptr]->category_id = $existingCat->category_id;
                \DB::table('categories')->where('ptr', $v->ptr)->update(['temp' => 1, 'status' => $this->status]);
                continue;
            } else {
                $this->attempts = 0;
                $this->nodesByPtr[$v->ptr]->category_id = $this->createCategoryFromNode($v);
            }
        }

        foreach($this->nodesByPtr as $node) {
            if($node->parent_ptr) {
                $parent = \DB::table('categories')->where('ptr', $node->parent_ptr)->first();
                if(!$parent) {
                    dd('no parent with ptr ' .$node->parent_ptr.PHP_EOL);
                } else {
                    $parent = $parent->category_id;
                }
            } else {
                $parent = 123;
            }
            \DB::table('categories')->where('ptr', $node->ptr)->update(['parent' => $parent, 'temp' => 1]);
        }

        echo $i;
    }

    function buildNewTree() {

        $this->nodes = $this->getParsedNodes();
        $this->nodesByPtr = [];

        foreach($this->nodes as $k => $v) {
            $this->nodesByPtr[$v->ptr] = $v;
        }

        foreach($this->nodes as $k => $v) {
            if($v->node_type == 'schematic') { continue; }
            $childs = \json_decode($v->childs);
            if(empty($childs)) { continue; }
            foreach($childs as $child) {
                $childPtr = $child[0];
                if(!isset($this->nodesByPtr[$childPtr])) {

                }
                $this->nodesByPtr[$childPtr]->parent = $v->ptr;
            }
        }

        foreach($this->nodesByPtr as $node) {
            if(!@$node->parent) { continue; }
            echo 'setting parent for '.$node->ptr.PHP_EOL;
            \DB::table('outsource_ezpartscatalog_nodes')
            ->where('ptr', $node->ptr)->update([
            'parent_ptr' => $node->parent
            ]);
        }

    }

    function markNodeAsParsed($ptr) {
        \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $ptr)->update(['parsed_at' => date('Y-m-d H:i:s')]);
    }

    function recursiveGetNodesOfNode($ptr) {
        $dbNode = \DB::table('outsource_ezpartscatalog_nodes')->select('ptr', 'node_type', 'childs', 'childs_parsed_time', 'parsed_at')->where('ptr', $ptr)->first();
        echo 'scanning node ' .$ptr.PHP_EOL;
        //if($dbNode->parsed_at) { return 1; }
        if(!empty(trim($dbNode->node_type))) {
            if(!in_array($dbNode->node_type, $this->scannableNodeTypes)) {
                $this->markNodeAsParsed($ptr);
                return 1;
            }
        }
        $response = \json_decode($dbNode->childs);
        if(empty($response)) {
            $response = $this->getNodeByPtr($ptr, $dbNode->node_type);
        }
        if((!empty($response))) {
            foreach($response as $item) {
                $itemptr = $item[0];
                $title = $item[1];
                $type = strtolower($item[2]);
                $this->nodeType = $type;
                if($this->nodeExists($itemptr)) {
                    echo 'node '.$itemptr. ' is '.$type.PHP_EOL;
                    \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $itemptr)->update(['node_type' => strtolower($type)]);
                    $this->recursiveGetNodesOfNode($itemptr);
                    continue;
                }
                echo 'node '.$itemptr. ' is '.$type.PHP_EOL;
                if(!$this->forgeItem($itemptr, $title, $type, $dbNode->ptr)) {
                    echo 'WARNING, COUDLN\' CREATE NODE '.$ptr.PHP_EOL;
                    continue;
                }
                $this->recursiveGetNodesOfNode($itemptr);
            }
        }
        $this->markNodeAsParsed($ptr);
        return 1;
    }

    function forgeItem($ptr, $title, $type, $parent) {
        try {
            \DB::table('outsource_ezpartscatalog_nodes')->insert([
            'ptr' => $ptr, 'title' => $title, 'source' => 2,
             'node_type' => strtolower($type), 'parent_ptr' => $parent
            ]);
        } catch(\PDOException $e) {
            echo "coudn't create node with ptr ".$ptr.PHP_EOL;
            return 0;
        }
        echo 'node '.$ptr.' '.$title.' has been created'.PHP_EOL;
        $node = \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $ptr)->first();
        $this->parseNodeByPtr($node);
        return 1;
    }

    function getNodeByPtr($ptr, $type) {
        $opts = ['X-AjaxPro-Method' => 'getChildNodes', 'Accept' => '*/*'];
        $modelPtr = (strtolower($type) == 'model') ? $ptr : null;
        $post = \json_encode(['nodePtr' => $ptr, 'modelPtr' => $modelPtr]);
        echo 'trying to scan ptr '.$ptr.PHP_EOL;
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxTreeControl,App_Web_rxddsmh4.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
        $response = \json_decode($response);
        $responseVal = @$response->value;
        if(!empty($responsveVal)) {
            echo 'node '.$ptr . ' has been scanned'.PHP_EOL;
        } else {
            if(in_array($this->nodeType, $this->scannableNodeTypes)) {
                echo 'WARNING ||| '.$this->nodeType.' with ptr ' .$ptr .' is empty'.PHP_EOL;
                dd($responseVal);
              //  die;
            } else {
                echo $this->nodeType. ' '.$ptr. ' is empty'.PHP_EOL;
            }
        }

        \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $ptr)->update(['childs' => \json_encode($responseVal), 'childs_parsed_time' => date('Y-m-d H:i:s')]);
        return $responseVal;
    }

    function nodeExists($ptr) {
        return \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $ptr)->count();
    }

    function productExist($ptr) {
        return \DB::table('outsource_ezpartscatalog_parts')->where('ptr', $ptr)->count();
    }

    function getRootNodes() {
        $nodes = [
            '24554328245469', '24554328165195', '24554328245464', '24554328245479', '24554328278282',
            '24554328245482', '24554328140121', '24554328354363', '24554328245478'
        ];
        return $nodes;
    }

    function getNodesOfNode($ptr) {
        $opts = ['X-AjaxPro-Method' => 'getChildNodes', 'Accept' => '*/*'];
        $post = \json_encode(['nodePtr' => $ptr, 'modelPtr' => null]);
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxSearchControl,App_Web_rxddsmh4.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
        $response = \json_decode($response);
        if(!$response || !@count(@$response->value->DataCache)) { return false; }
        return $response;
    }

    function createNodeFromResponse(){

    }

    function getNodesOfParts() {
        $parts = \DB::table('outsource_ezpartscatalog_parts')
        ->whereNull('part_data_date')
        ->where('ptr', '24588687834503')
        ->get();
        $this->auth($this->bot);
        foreach($parts as $k => $v) {
            $this->getNodesOfPart($v->ptr);
        }
    }

    function createTmpCookie() {
        $filename = date('H-i-s');
        $path = $this->cookiesTmp.$filename.'.txt';
        @unlink($path);
        $file = fopen($path, 'w+');
        fclose($file);
        chmod($path, 0777);
        return $path;
    }

    function clear() {
        $files = scandir($this->cookiesTmp);
        $files = array_diff($files, ['.', '..']);
        foreach($files as $x) {
            @unlink($this->cookiesTmp.$x);
        }
    }

    function setRandomCookie($bot) {
        $tmpCookie = realpath($this->createTmpCookie());
        curl_setopt($bot, CURLOPT_COOKIEFILE, $tmpCookie);
        curl_setopt($bot, CURLOPT_COOKIEJAR, $tmpCookie);
    }

    function getPartsByCodes($filepath) {
        $partCodes = \json_decode(file_get_contents($filepath));
        $existing = $this->ignoreExists ? \arrayMap(\DB::table('parser_ezpartscatalog_parts')->get(), 'code') : 0;
        foreach($partCodes as $code) {
            if($this->ignoreExists && isset($existing[$code])) { continue; }
            // for multithread
            if($this->ignoreExists && \DB::table('parser_ezpartscatalog_parts')->where('code', $code)->first()) { continue; }
            $part = $this->getPartByCode($code, $this->bot);
            if(!$part) { continue; }
            try {
                \DB::table('parser_ezpartscatalog_parts')->insert((array)$part);
            } catch(\PDOException $e) {
                echo $e->getMessage(); echo '<br>';
            }
        }


        $this->clear();

    }

    function getDataByPartCodes($filepath) {
        $this->auth($this->bot);
        $this->getPartsByCodes($filepath);
    }

    function translateParts() {
        $json = \json_decode(file_get_contents(public_path().'/../resources/schedules/ezPartsParser/partsRus.json'));
        if(!$json) { return; }
        foreach($json as $k => $v) {
           \DB::table('parser_ezpartscatalog_parts')->where('ptr', $v->Part_Ptr)->update(['title' => $v->Description]);
        }
    }

    function devx() {
        $this->auth($this->bot);
        $schemes = \DB::table('products')
            ->select('products.product_id', 'outsource_ezpartscatalog_nodes.*')
            ->join('outsource_ezpartscatalog_nodes', 'outsource_ezpartscatalog_nodes.ptr', '=', 'products.ptr')
            //->whereRaw(\DB::raw('resource_data_parsed > current_date - 1'))
           // ->where('resource_data', '<>', '[]')
            //->take(10000)
            ->get();
        $i = 0;
        if(empty($schemes)) { return; }
        curl_setopt($this->bot, CURLOPT_TIMEOUT, 20);
        foreach($schemes as $x) {
            $isParent = \DB::table('product_consistance')->where('parent_product', $x->product_id)->get();
            if ($isParent->count()) {
                continue;
            }
            $i++;
            //$this->parseNodeResourceData($x);
           // continue;
            $this->parseNodeDataGroups($x);

        }
        echo $i;
    }

    function parseNodeResourceData(&$x) {
        $post = '{"nodePtr":"'.$x->ptr.'"}';
        $opts = ['X-AjaxPro-Method' => 'getLoadableResourcesInfo', 'Accept' => '*/*'];
        curl_setopt($this->bot, CURLOPT_POST, 1);
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxTreeControl,App_Web_rxddsmh4.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
        $response = \json_decode($response);
        $update = \json_encode(@$response->value);
        $x->resource_data = $update;
        \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(['resource_data' => $update]);
        return $response;
    }

    function findEmptySchemes() {
        $schemes = \DB::table('products')
            ->join('outsource_ezpartscatalog_nodes', 'outsource_ezpartscatalog_nodes.ptr', '=', 'products.ptr')
            ->get();
        $i = 0;
        foreach($schemes as $x) {
            $isParent = \DB::table('product_consistance')->where('parent_product', $x->product_id)->get();
            if($isParent->count()) { continue; }
            $i++;
            echo $x->product_id;
            echo '<br>';
        }
        echo $i;
    }


    function parseNodesResourceData() {
        $this->auth($this->bot);
        $schemes = \DB::table('outsource_ezpartscatalog_nodes')
            ->select('products.ptr', 'products.avatar', 'products.product_id',
                'outsource_ezpartscatalog_nodes.resource_data_http_code', 'outsource_ezpartscatalog_nodes.resource_data')
            ->join('products', 'products.ptr', '=', 'outsource_ezpartscatalog_nodes.ptr')
            ->where('outsource_ezpartscatalog_nodes.ptr', '24554328245469')
            ->get();
        if(!$schemes->count()) { return; }
        curl_setopt($this->bot, CURLOPT_TIMEOUT, 20);
        foreach($schemes as $x) {
            $this->parseNodeResourceData($x);
        }
    }

    function parseDataGroups() {
            $this->auth($this->bot);
            $nodes = \DB::table('outsource_ezpartscatalog_nodes')
            ->select('products.ptr', 'products.avatar', 'products.product_id', 'outsource_ezpartscatalog_nodes.data_group',
                'outsource_ezpartscatalog_nodes.resource_data_http_code', 'outsource_ezpartscatalog_nodes.data_group_http_code', 'outsource_ezpartscatalog_nodes.resource_data')
                ->join('products', 'products.ptr', '=', 'outsource_ezpartscatalog_nodes.ptr')
                ->get();
            foreach($nodes as $x) {
                $dg = \json_decode($x->data_group);
                if(!empty($dg->value[0]->DataCache)) { continue; }
                if(!empty($dg->error)) { continue; }
                $this->parseNodeDataGroups($x);
            }
            $i = 0;
            echo $i;
    }

    function parseHotSpots() {
        $this->auth($this->bot);
        $nodes = \DB::table('outsource_ezpartscatalog_nodes')
        ->select('products.ptr', 'products.avatar', 'products.product_id',
            'outsource_ezpartscatalog_nodes.resource_data_http_code', 'outsource_ezpartscatalog_nodes.data_group_http_code',
            'outsource_ezpartscatalog_nodes.resource_data')
            ->join('products', 'products.ptr', '=', 'outsource_ezpartscatalog_nodes.ptr')
            ->get();
        foreach($nodes as $x) {
            $this->parseHotSpotsByNode($x);
        }
    }

    function parseHotSpotsByNode($x) {
        $resourceData = \json_decode($x->resource_data);
        if(!@$resourceData[0]->ResourcePtr) {
            \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(['hotspot_scanned' => date('Y-m-d H:i:s')]);
            return;
        }
        $post = json_encode(['resourcePtr' => (string)$resourceData[0]->ResourcePtr]);
        $opts = ['X-AjaxPro-Method' => 'getSchematicOptions', 'Accept' => '*/*'];
        curl_setopt($this->bot, CURLOPT_POST, 1);
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxPartsGridControl,App_Web_5o1fpeuv.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
        $response = \json_decode($response);
        $update = \json_encode($response);
        \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(['data_group' => $update, 'data_group_scanned' => date('Y-m-d H:i:s'), 'data_group_http_code' => curl_getinfo($this->bot, CURLINFO_HTTP_CODE)]);
        return $response;
    }

    function parseNodeDataGroups(&$x) {

        $resourceData = \json_decode($x->resource_data);
        if(!@$resourceData[0]->ResourcePtr) {
            return;
        }
        $post = json_encode(['resourcePtr' => (string)$resourceData[0]->ResourcePtr]);
        $opts = ['X-AjaxPro-Method' => 'getDataGroups', 'Accept' => '*/*'];
        curl_setopt($this->bot, CURLOPT_POST, 1);
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxPartsGridControl,App_Web_rxddsmh4.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
        $response = \json_decode($response);
        $update = \json_encode($response);
        $x->data_group = $update;
        \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(['data_group' => $update]);
        return $response;
    }

    function unpackDataGroups() {
        $dg = \DB::table('outsource_ezpartscatalog_nodes')->where('data_group_http_code', 200)
            ->get();
        foreach($dg as $x) {
            $dataGroup  = \json_decode($x->data_group);
            if(!empty($dataGroup->error)) { continue; }
            if(empty($dataGroup->value[0]->DataCache)) { continue; }
            $values = $dataGroup->value[0]->DataCache;
            dd($values);
        }

    }

    function handleDataGroupInfo(&$x) {

            $rd = \json_decode($x->data_group);
            $parentProduct = \DB::table('products')->where('ptr', $x->ptr)->first();
            if(!$parentProduct) { return; }
            if(!empty($rd->error)) { return; }
            if(empty($rd->value[0]->DataCache)) { return; }
            foreach($rd->value[0]->DataCache as $k => $v) {
                $displayData = $v[1]; // данные для дисплея
                $partData = $v[3]; // данные о запчасти
                $status = $v[4];
                $part = \DB::table('products')->where('ptr', $partData->PartPtr)->first();
                if(!$part) {
                    echo 'warn!'.PHP_EOL;
                    continue;
                }
                try {
                    \DB::table('product_consistance')
                        ->insert(['parent_product' => $parentProduct->product_id,
                            'child_product' => $part->product_id,
                            'ordering' => $k,
                            'consistance_quantity' => $displayData[5],
                            'scheme' => \json_encode([
                                'number' => $displayData[2],
                                'hint' => $displayData[7],
                                'status' => $status
                            ])
                        ]);
                        echo 'consistances has been created for product ' .$part->product_id;
                } catch(\PDOException $e) {
                  //  dd($e->getMessage());
                 }
                /*
                    $displayData[2];  номер на схеме
                    $displayData[3];  Класс запчасти
                    $displayData[4];  Артикул запчасти
                    $displayData[5];  Количество
                    $displayData[6];  название
                    $displayData[7];  Примечание
                    $partData->PartPtr;  Птр запчасти
                */
            }
    }

    function getSchematicImages() {
        $this->i = 0;
        $this->auth($this->bot);
        $nodes = \DB::table('outsource_ezpartscatalog_nodes')
            ->select('products.ptr', 'products.avatar', 'products.product_id',
                'outsource_ezpartscatalog_nodes.resource_data_http_code', 'outsource_ezpartscatalog_nodes.data_group_http_code',
                'outsource_ezpartscatalog_nodes.resource_data')
            ->join('products', 'products.ptr', '=', 'outsource_ezpartscatalog_nodes.ptr')
            ->get();
        foreach($nodes as $x) {
            if(empty($x->avatar)) { continue; }
            if(!is_file('../../nwstorage/public/i/nwmotors/products/catalog/'.$x->avatar)) {
                $this->getSchematicImage($x);
            }
        }
        echo 'done';
    }

    function getSchematicImage(&$x) {
        $jrd = \json_decode($x->resource_data);
        if(empty($jrd[0]->ResourceVersionPtr)) {
            $x->avatar_downloaded = '';
            $x->avatar_http_code = 999;
            $x->reason = 'there is no resource data';
            \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(['avatar_http_code' => $x->avatar_http_code]);
            return;
        }
        $post = json_encode(['schematicPtr' => $jrd[0]->ResourcePtr, 'schematicScale' => 100]);
        $opts = ['X-AjaxPro-Method' => 'getSchematicImage', 'Accept' => '*/*'];
        curl_setopt($this->bot, CURLOPT_POST, 1);
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxSchematicControl,App_Web_rxddsmh4.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
        $x->reason = $response;
        $x->avatar_data = \json_encode($response);
        $x->avatar_http_code = curl_getinfo($this->bot, CURLINFO_HTTP_CODE);
        \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(['avatar_http_code' => $x->avatar_http_code, 'avatar_data' => $x->avatar_data]);
        $preg = preg_match("/('(.*?)')/s", $response, $matches);
        if(!count($matches)) { return; }
        $first = trim($matches[2], "'");
        curl_setopt($this->bot, CURLOPT_HTTPGET, 1);
        curl_setopt($this->bot,CURLOPT_POSTFIELDS, null);
        $url = 'https://public-mercurymarine.sysonline.com/ajaximage/'.$first.'.ashx';
        curl_setopt($this->bot, CURLOPT_URL, $url);
        curl_setopt($this->bot ,CURLOPT_ENCODING , null);
        $opts = ['X-Requested-With' => 'XMLHttpRequest', 'Accept' => 'image/webp,image/*,*/*;q=0.8'];
        $response = $this->request($opts, $this->bot);
        file_put_contents(public_path().'/../resources/dev/resource_avatars/'.$first.'.jpg', $response);
        echo 'avatar for ' .$x->ptr. ' has been downloaded'.PHP_EOL;
        $x->avatar_downloaded = $first.'.jpg';
        \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(['avatar_downloaded' => $x->avatar_downloaded]);
        return $response;
        // отправляем ресурс версион птр
    }




    function getNodeHotSpots($x) {
        $data = \json_decode($x->data);
        if(empty($data->value[0]->ResourcePtr)) { $this->i++;  }
        return;
        $post = json_encode(['resourcePtr' => $pa, "curModelPtr" => null]);
        $opts = ['X-AjaxPro-Method' => 'getHotSpots', 'Accept' => '*/*'];
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxSearchControl,App_Web_r5ymwjnn.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
        $response = \json_decode($response);
        if(!$response || !@count(@$response->value->DataCache)) { $this->noProductResources[$partId] = 1; return false; }
        return $response;
    }

    function getNodeResources() {
        $nodes = \DB::table('outsource_ezpartscatalog_nodes')->whereNull('ignore')->whereNull('data')
        ->whereNull('data_scanned')->where('is_final', 1)->orderBy('ptr', 'desc')
        //->where('template', '24708946853889')
        ->get();
        $this->auth($this->bot);

        foreach($nodes as $x) {
            \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(
            ['data' => $this->getNodeResource($x), 'data_scanned' => date('Y-m-d H:i:s')]);
        }
    }

    function getNodeResource($node) {
        $post = json_encode(['nodePtr' => $node->ptr]);
        $opts = ['X-AjaxPro-Method' => 'getLoadableResourcesInfo'];
        curl_setopt($this->bot, CURLOPT_POST, 1);
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxTreeControl,App_Web_r5ymwjnn.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
       // print_r($response); die;
        $response = \json_decode($response);
        return $response;
    }


}

