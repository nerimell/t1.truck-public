<?php

namespace App\Models\Schedules;
use App\Models\ManufacturerModel;

class Synchronization {

    public $products;
    public $currentExistingProduct;
    public $toTable;

    function sync() {
        $this->updateManufacturers();
        $this->updateProducts();
    }

    function updateProducts() {
        //$this->products = \DB::select("SELECT * FROM outsource_1c_products WHERE dest_updated_at is null OR source_updated_at > dest_updated_at");
        $this->products = \DB::select("SELECT * FROM outsource_1c_products WHERE dest_updated_at = '2017-01-01 00:00:00'");
        foreach($this->products as $k => $v) {
            $this->syncProduct($v);
        }
    }

    function selectTitle($longtitle, $title) {
        $aLength = mb_strlen($longtitle);
        $bLength = mb_strlen($title);
        if($aLength > $bLength) { return $longtitle; }
        if($bLength - $aLength > 12) { return $title; }
        return $longtitle;
    }

    function syncProduct($obj) {
        $this->currentExistingProduct = \DB::table('products')->select('product_id')->where('storage_id', $obj->guid)->first();
        $this->toTable = [
            'description' => $obj->description,
            'title' => $this->selectTitle($obj->longtitle, $obj->title),
            'width' => $obj->width,
            'height' => $obj->height,
            'length' => $obj->depth,
            'meta_title' => $obj->meta_title,
            'meta_description' => $obj->meta_description,
            'status' => (bool)$obj->status,
            'brutto' => $obj->brutto,
            'netto' => $obj->netto,
            'barcode' => $obj->barcode,
            'volume' => $obj->volume
        ];
        $priceinfo = $this->reformatPrices($obj);
        if($this->currentExistingProduct) {
            $this->updateProductRow($obj);
        } else {
            //$this->createProductRow($obj);
        }
    }

    function reformatPrices($obj) {

    }

    function updateProductRow($obj) {
        \DB::table('products')->where('product_id', $this->currentExistingProduct->product_id)
        ->update($this->toTable);
    }

    function createProductRow($obj) {
        \DB::table('products')->insert([

        ]);
    }

    function updateManufacturers() {
        $uniqueManufacturers = \DB::select('SELECT DISTINCT manufacturer_guid, manufacturer_title FROM outsource_1c_products');
        if(empty($uniqueManufacturers)) { return; }
        foreach($uniqueManufacturers as $k => $v) {
            $v = $this->trimFields($uniqueManufacturers[$k]);
            if(empty($v->manufacturer_guid) || empty($v->manufacturer_title)) { continue; }
            $manufacturer = $this->getManufacturerByGuid($v->manufacturer_guid);
            $this->attempts = 0;
            $v->manufacturer_title = trim($v->manufacturer_title);
            $this->updateManufacturer(@$manufacturer, $v);
        }
    }

    function getManufacturerByGuid($guid) {
        return \DB::table('manufacturers')->where('storage_id', $guid)->first();
    }

    function trimFields(&$a) {
        foreach($a as $k => $v) {
            $a->{$k} = trim($v);
        }
        return $a;
    }



    function updateManufacturer($old, $obj) {
        if(empty($obj->manufacturer_title)) { return 0; }
        if(!$old) {
            try {
                $alias = \Str::slug($obj->manufacturer_title);
                $alias = $this->attempts ? $alias . '-'.$this->attempts : $alias;
                \DB::table('manufacturers')->insert(['title' => $obj->manufacturer_title, 'storage_id' => $obj->manufacturer_guid, 'alias' => $alias]);
            } catch(\Exception $e){
               if($this->attempts > 100) { return 0; }
               $this->attempts++;
               return $this->updateManufacturer($old, $obj);
            }
        } else {
            return $this->updateManufacturerRow($obj);
        }
    }

    function updateManufacturerRow($obj) {
        return \DB::table('manufacturers')->where('storage_id', $obj->manufacturer_guid)->update(['title' => $obj->manufacturer_title]);
    }
}