<?php
namespace App\Models\Deprecated;

class Model {

    public $perPage = 21;
    public $params;
    public $fields;
    public $data;

    function collectData() {
        if(empty($this->fields)) { return; }
        $this->data = [];
        foreach($this->fields as $k) {
            $this->data[$k] = \Request::get($k);
        }
    }
}

?>