<?php

namespace App\Models\Deprecated;

class DevModel {

  public $cm;
  public $attempts = 0;

  function changeCategoryLink($id) {
    $cat = \DB::table('categories')->where('category_id', $id)->first();
    if(!$cat) { return; }
    $this->updateCatLink($cat);
  }

  function updateCatLink($category) {
    $cm = CategoriesModel::getInstance();
    if(!empty($category->link_id)) { \DB::table('links')->where('id', $category->link_id)->delete(); }
    $category = $cm->getCategoryById($category->category_id);
    $link_id = \DB::table('links')->insertGetId(['link' => trim($category->link, '/'), 'source' => 'category']);
    \DB::table('categories')->where('category_id', $category->category_id)->update(['link_id' => $link_id]);
    $cm::$_instance = null;
    $childProducts = \DB::table('product_categories')->where('category_id', $category->category_id)->get();
    if(!empty($childProducts)) {
      foreach($childProducts as $product) {
        $this->attempts = 0;
        $this->updateProductLink($product);
      }
    }
    $childCategories = \DB::table('categories')->where('parent', $category->category_id)->get();
    if(!empty($childCategories)) {
      foreach($childCategories as $child) {
        $this->updateCatLink($child);
      }
    }
  }

  function updateProductLink($product) {
    $pm = ProductsModel::getInstance();
    $pm->getProductById($product->product_id);
    $product = $pm->product;
    $link_id = $this->getProductLink($product);
    \DB::table('products')->where('product_id', $product->product_id)->update(['link_id' => $link_id]);
    ProductsModel::$_instance = null;
    CategoriesModel::$_instance = null;
  }

  function getCatLink($category) {
      $link = $this->attempts ? $category->link .'-'.$this->attempts : $category->link;
      try {
          $link_id = \DB::table('links')->insertGetId(['link' => trim($link, '/'), 'source' => 'category']);
      } catch(\Exception $e) {
          $this->attempts++;
          return $this->getCatLink($category);
      }
      return $link_id;
  }

  function getProductLink($category) {
      $link = $this->attempts ? $category->link .'-'.$this->attempts : $category->link;
      try {
          $link_id = \DB::table('links')->insertGetId(['link' => trim($link, '/'), 'source' => 'product']);
      } catch(\Exception $e) {
          $this->attempts++;
          return $this->getProductLink($category);
      }
      return $link_id;
  }

/*
  function createLinksForProducts() {
      $products = \DB::table('products')->select('product_id')->get();
      foreach($products as $product) {
          $this->attempts = 0;
          $pm = ProductsModel::getInstance();
          $pm->getProductById($product->product_id);
          $product = $pm->product;
          $link_id = $this->getLink($product);
          \DB::table('products')->where('product_id', $product->product_id)->update(['link_id' => $link_id]);
          ProductsModel::$_instance = null;
          CategoriesModel::$_instance = null;
      }
  }

  function createLinksForCategories($ids = false) {
      $q = \DB::table('categories')->select('category_id');
      if($ids) { $q->whereIn('category_id', $ids); }
      $cats = $q->get();
      foreach($cats as $x) {
          $cm = CategoriesModel::getInstance();
          if(!empty($x->link_id)) { \DB::table('links')->where('id', $x->link_id)->delete(); }
          $category = $cm->getCategoryById($x->category_id);
          $link_id = \DB::table('links')->insertGetId(['link' => trim($category->link, '/'), 'source' => 'category']);
          \DB::table('categories')->where('category_id', $x->category_id)->update(['link_id' => $link_id]);
          $cm::$_instance = null;
      }
  }
  */



}
