<?php

namespace App\Models;

class MailerModel {

    public $settings;
    public $productsModel;
    public $inited = 0;

    function init() {
        if($this->inited) { return; }
        $this->settings = (SettingsModel::getInstance())->getSiteData();
        $this->productsModel = ProductsModel::getINstance();
        $this->inited = 1;
    }

    function getHeaders() {
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'From: '.$this->settings->mailFrom."\r\n".
            'Reply-To: '.$this->settings->mailFrom."\r\n" .
            'X-Mailer: PHP/' . phpversion();
        return $headers;
    }

    function sendEmails($params) {
        $headers = $this->getHeaders();
        foreach($this->settings->notifyEmails as $x) {
            mail($x, $params->title, $params->html, $headers);
        }
    }

    function sendExternalProduct($x) {
        $this->init();
        if(!$this->productsModel->getProductById($x->product_id)) { return; }
        $x->product = $this->productsModel->product;
        $x->user = UsersModel::getUserById($x->user_id);
        $params = (object)['data' => $x, 'html' => view('messages.externalProduct', ['data' => $x])->render(), 'title' =>'Клиент нашел более дешевый товар, чем на сайте nwmotors.ru'];
        $this->sendEmails($params);
        \DB::table('external_offers')->where('external_offer_id', $x->external_offer_id)->update(['status' => 1]);
    }

    function sendFormedOrder($order) {

    }

}