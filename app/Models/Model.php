<?php
namespace App\Models;

class Model {

    public $perPage = 24;
    public $params;
    public $fields;
    public $data;
    public $result;

    function collectData() {
        if(empty($this->fields)) { return; }
        $this->data = [];
        foreach($this->fields as $k) {
            $this->data[$k] = \Request::get($k);
        }
    }
}
