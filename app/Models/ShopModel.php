<?php

namespace App\Models;
use App\Models\Instances\Cart;
use App\Models\Instances\Priceinfo;
use App\Models\UsersModel;
use App\Models\DeliveryModel;

class ShopModel extends Model {

    public $productsModel;
    public $fields;
    public $q;
    public static $_instance = null;
    public $pricesDecoded = 0;
    public $inited;
    public $priceTable = 'products.priceinfo';

    // cart
    public $cart = null;
    public $cookieName;
    public $cookieCart;
    public $postCookieCart;
    public $cookieCartProductsById = [];
    public $cookieCartComplexesById = [];
    public $cartProductsById = [];
    public $cartComplexesByIds = [];
    public $types = ['c', 'p']; // c = complex
    public $productsLimit = 99;
    public $insertedOrder;
    public $deletedProduct;
    public $deletedComplex;
    public $selectedGift;
    public $selectedGiftIndex;
    public $complexIds = [];
    public $productIds = [];
    public static $myOrders;
    public $orderCity;
    public static $currentOrder;
    public static $currentOrderId;
    public $paymentMethods;

    // discounts
    public static $coupon;
    public static $shopperGroup;
    public static $discountCalculated;
    public $attempts;
    public $orderStatuses;
    public $codesByProducts;

    // orders
    public $hash;

    private function __construct() {

    }

    static function getInstance() {
        if(self::$_instance != null) { return self::$_instance; }
        return self::$_instance = new self;
    }

    function init() {
        if($this->inited) { return; }
        $this->productsModel = ProductsModel::getInstance();
        $this->cookieName = 'cart';
        $this->orderCity = \Request::get('order_city');
        if(!self::$currentOrderId) {
            self::$currentOrderId = session('currentOrderId');
        }
        if($this->orderCity) {
            \Html::$city = getCityById($this->orderCity);
        }
        $this->inited = 1;
    }

    function validateCookieProduct(&$x) {
        if(!isset($x->p)) { return 0; }
        if(!\anInt($x->p)) { return 0; }
        if(isset($x->t) && $x->t) {
            if(!in_array($x->t, $this->types)) { return 0; }
        } else {
            unset($x->t);
        }
        if(!isset($x->q)) { $x->q = 1; return 1; }
        if(!\anInt($x->q)) { return 0; }
        if($x->q > $this->productsLimit) { $x->q = $this->productsLimit; return 1; }
        return 1;
    }

    function clearCartStats() {
          $this->cart->totals->products = 0;
          $this->cart->totals->base_price = 0;
          $this->cart->totals->final_price = 0;
          $this->cart->totals->discount = 0;
          $this->cart->totals->instances = 0;
          $this->cart->totals->gifts = 0;
    }

    function formatInstancesToCookieCart($instances) {
        $this->result();
        $result = [];
        if(empty($instances)) { return $result; }
        foreach($instances as $instance) {
            if(!$this->validateCalculationInstance($instance)) { return 0; }
            $result[] = (object)['p' => $instance->instanceId, 't' => @$instance->t, 'q' => @$instance->quantity];

        }
        return $result;
    }

    function getCurrentOrder() {
        self::$currentOrder = \DB::table('tmp_orders')->where('tmp_order_id', self::$currentOrderId)->first();
    }

    function getCookieCart() {
        $this->getCurrentOrder();
        $this->cookieCart = $this->postCookieCart ? $this->postCookieCart : toArray(\json_decode(@self::$currentOrder->order_data));
        $this->cookieCartComplexesById = [];
        $this->cookieCartProductsById = [];
        $productIdsSolved = []; $complexIdsSolved = [];
        $total = count($this->cookieCart);
        for($i = 0; $i < $total; $i++) {
            if(!$this->handleCartInstance($this->cookieCart[$i])) { continue; }
            if(@$this->cookieCart[$i]->t == 'c') {
                if(isset($this->cookieCarComplexesById[$this->cookieCart[$i]->p])) { $this->cookieCartComplexesById[$this->cookieCart[$i]->p]->q += $this->cookieCart[$i]->q; continue; }
                $this->cookieCartComplexesById[$this->cookieCart[$i]->p] = &$this->cookieCart[$i]; continue;
            }
            if(isset($this->cookieCartProductsById[$this->cookieCart[$i]->p])) { $this->cookieCartProductsById[$this->cookieCart[$i]->p]->q += $this->cookieCart[$i]->q; continue; }
            $this->cookieCartProductsById[$this->cookieCart[$i]->p] = &$this->cookieCart[$i];
        }
        $r = [];
        $total = count($this->cookieCart);
        for($i = 0; $i < $total; $i++) {
            if(@$this->cookieCart[$i]->t == 'c') {
                if(in_array($this->cookieCart[$i]->p, $complexIdsSolved) || !isset($this->cookieCartComplexesById[$this->cookieCart[$i]->p])) { continue; }
                $r[] = $this->cookieCartComplexesById[$this->cookieCart[$i]->p];
                $complexIdsSolved[] = $this->cookieCart[$i]->p;
                continue;
            }
            if(in_array(@$this->cookieCart[$i]->p, $productIdsSolved) || !isset($this->cookieCartProductsById[@$this->cookieCart[$i]->p])) { continue; }
            $r[] = $this->cookieCartProductsById[$this->cookieCart[$i]->p];
            $productIdsSolved[] = $this->cookieCart[$i]->p;
        }
        $this->postCookieCart = $r;
        return $this->cookieCart = $this->postCookieCart;
    }

    function handleCartInstance(&$item) {
        if(!$this->validateCookieProduct($item)) { unset($item); return 0; }
        if(@$item->t == 'c') {
            if($this->deletedComplex == $item->p) { unset($item); return 0; }
            $this->complexIds[] = $item->p;  return 1;
        }
        if($this->deletedProduct == $item->p) { unset($item); return 0; }
        $this->productIds[] = $item->p;
        return 1;
    }

    function getCart() {
        $this->init();
        if(!is_null($this->cart)) { return $this->cart; }
        $this->cart = new Cart;
        $this->cookieCart = $this->getCookieCart();
        if(is_array($this->cookieCart) && !empty($this->cookieCart)) {
            $this->productIds = array_unique($this->productIds); $this->complexIds = array_unique($this->complexIds);
            $this->queryCartInstances();
        }
        $this->handleDatabaseInstances();
        $this->recalculateCart();

        return $this->cart;
    }

    function handleDatabaseInstances() {
        if(empty($this->cookieCart) || (empty($this->cart->products) && empty($this->cart->complexes))) { return; }
        foreach($this->cookieCart as $key => $instance) {
            $instance->q = (int)@$instance->q;
            $instance->giftsHandled = @$instance->giftsHandled;
            if(@$instance->t == 'c') {
                if(!isset($this->cartComplexesByIds[$instance->p])) {
                    unset($this->cookieCart[$key]);
                    continue;
                }
                $this->cart->instances[] = &$this->cartComplexesByIds[$instance->p];
                $this->cartComplexesByIds[$instance->p]->q = $instance->q > $this->productsLimit ? $this->productsLimit : $instance->q;
                $this->cartComplexesByIds[$instance->p]->t = 'c';
                $this->formatGifts($this->cartComplexesByIds[$instance->p]);
                continue;
            }
            if(!isset($this->cartProductsById[$instance->p])) {
                unset($this->cookieCart[$key]);
                continue;
            }
            $this->cartProductsById[$instance->p]->q = $instance->q > $this->productsLimit ? $this->productsLimit : $instance->q;
            $this->formatGifts($this->cartProductsById[$instance->p]);
            $this->cart->instances[] = &$this->cartProductsById[$instance->p];
        }


    }

    static function getComplexesByIds($ids, $gifts = 1) {
        $giftsCol = $gifts ? 'products.gifts, ' : '';
        $ids = join(',', $ids);
        $sqlComplexes = \DB::select("SELECT products.title, ".$giftsCol." concat((SELECT getProductPathById(products.product_id)), '/', products.alias) as link, ".ProductsModel::getCoalesce()." as priceinfo, products.product_id, products.avatar, 
           product_complexes.discount, product_complex_consistance.complex_id, products.width, products.height,
            products.length, products.netto, products.brutto, product_complex_consistance.ordering FROM products,
           product_complexes, product_complex_consistance WHERE  
           product_complex_consistance.complex_id IN (".$ids.") AND product_complexes.complex_id IN (".$ids.")
           AND products.status = true AND products.sells = true AND 
           product_complex_consistance.product_id = products.product_id 
           ORDER BY product_complex_consistance.ordering");
        return productsModel::handleSqlComplexes($sqlComplexes);


    }

    function queryCartInstances() {
        if(!empty($this->productIds)) {
            $this->cart->products =  \DB::table('products')
                ->whereIn('products.product_id', $this->productIds)
                ->select(\DB::raw("products.product_id, products.title, concat((SELECT getProductPathById(products.product_id)), '/', products.alias) as link, products.gifts,
                 products.width, products.length, products.height, products.netto, products.brutto,  
                    ".ProductsModel::getCoalesce()." as priceinfo, products.avatar"))
                ->get()->toArray();
            $total = count($this->cart->products);
            if(!$total) { return 1; }
            $gifts = [];
            for($i = 0; $i < $total; $i++) {
                if(isset($this->cartProductsById[$this->cart->products[$i]->product_id])) { continue; }
                $this->cart->products[$i]->giftsData = (object)\json_decode(@$this->cart->products[$i]->gifts);
                if(!empty($this->cart->products[$i]->giftsData->products)) {
                    foreach($this->cart->products[$i]->giftsData->products as $x) {
                        $gifts[] = $x;
                    }
                    $this->cart->products[$i]->cartGifts = (object)@$this->cookieCartProductsById[$this->cart->products[$i]->product_id]->g;
                } else {
                    $this->cart->products[$i]->cartGifts = new \stdClass;
                }
                $this->cartProductsById[$this->cart->products[$i]->product_id] = &$this->cart->products[$i];
            }
            //dd($this->cartProductsById);
          
        }

        if(!empty($gifts)) {
            $gifts = ProductsModel::getGiftsByIds($gifts);
            ProductsModel::linkGiftsWithInstances($gifts, $this->cart->products);
        }
        if(!empty($this->complexIds)) {
            $this->cart->complexes = self::getComplexesByIds($this->complexIds);
            $total = count($this->cart->complexes);
            for($i = 0; $i < $total; $i++) {
                if(!isset($this->cartComplexesByIds[$this->cart->complexes[$i]->complex_id])) {
                    $this->cartComplexesByIds[$this->cart->complexes[$i]->complex_id] = &$this->cart->complexes[$i];
                }
            }
        }
        return 1;
    }



    function validateProduct() {
        if(!\anInt($this->data['product_id'])) { $this->result->display = 'Неправильный формат данных о товаре'; return 0; }
        if(!\anInt($this->data['quantity'])) { $this->data['quantity'] = 1; }
        if($this->data['quantity'] > $this->productsLimit) { $this->result->display = 'Вы пытаетесь добавить слишком большое количество товара'; return 0; }
        return 1;
    }


    function addComplexToCart() {
        $this->init();
        $this->fields = ['complex_id'];
        $this->result = new \Result;
        $this->collectData();
        $complex = $this->getCartComplexById($this->data['complex_id']);
        if(!$complex) { $this->result->display = 'Не удалось найти комлпект'; return $this->result; }
        if(count($complex) != 1) { $this->result->display = 'Не удалось найти комплект'; return $this->result; }
        $this->cookieCart = $this->getCookieCart();
        if(isset($this->cookieCartComplexesById[$complex[0]->complex_id])) {
            $newquantity = (int)$this->cookieCartComplexesById[$complex[0]->complex_id]->q+1;
            if($newquantity > $this->productsLimit) {
                $this->result->display = 'Достигнут максимум для этого комплекта'; return $this->result;
            }
            $this->cookieCartComplexesById[$complex[0]->complex_id]->q = $newquantity;
        } else {
            $this->cookieCartComplexesById[$complex[0]->complex_id] = (object)['p' => $complex[0]->complex_id, 't' => 'c', 'q' => 1];
            $this->cookieCart[] = &$this->cookieCartComplexesById[$complex[0]->complex_id];
        }
        $this->postCookieCart = $this->cookieCart;
        $this->getCart();
        $this->buildNewCookieCart();
        $this->recalculateCart();
        $this->result->cart = $this->cart;
        $this->result->inner = view('elements.shop.cart-inner', ['cart' => $this->cart])->render();
        $this->result->display = 'Комплект добавлен в корзину';
        $this->result->result = 1;
        return $this->result;
    }

    function formatGifts(&$instance) {
        if(@$instance->giftsHandled) { return; }
        if(empty($instance->gifts)) {
            $instance->cartGifts = new \stdClass;
            $instance->giftsHandled = 1;
            return;
        }
        $giftsMap = [];
        foreach($instance->gifts as $k => $x) {
            $giftsMap[$x->product_id] = $k;
        }
        $defaultGifts = 0;
        $maxGifts = $instance->q;
        $giftsInProcess = 0;
        $cartGifts = new \stdClass;
        foreach($instance->giftsById as $k => $v) {
            if(!is_object($v->priceinfo)) {
                $v->priceinfo = \json_decode($v->priceinfo);
            }
            if(!@$v->priceinfo->price && !@$v->priceinfo->discount_price) {
                unset($instance->giftsById[$k]);
                unset($cartGifts->{$k}->product_id);
                unset($instance->gifts[$giftsMap[$k]]);
            }
        }
        $instance->gifts = array_values($instance->gifts);


        if(empty($instance->cartGifts)) {
            $instance->cartGifts = (object)[$instance->gifts[0]->product_id => $instance->q];
            $instance->giftsHandled = 1;

        } else {


            if(empty($instance->giftsById)) {
                $instance->cartGifts = null;
                $instance->gifts = null;
                $instance->cartGifts = null;
                $instance->giftsHandled = 1;
                return 1;
            }
            foreach($instance->cartGifts as $k => $x) {
                $x = (int)$x;
                $k = (int)$k;
                if(!isset($cartGifts->{$k})) { $cartGifts->{$k} = 0; }
                if(($x+$giftsInProcess) > $maxGifts) { $x = $maxGifts-$giftsInProcess; }
                if(!isset($instance->giftsById[$k])) {
                     $defaultGifts+=$x;
                     $giftsInProcess+=$x;
                     unset($cartGifts->{$k});
                     continue;
                }
                $cartGifts->{$k} = $x;
                $giftsInProcess+=$x;
            }

            $instance->giftsHandled = 1;
            if(@$instance->gifts[0]->product_id) {
                if(!isset($cartGifts->{@$instance->gifts[0]->product_id})) {
                    $cartGifts->{@$instance->gifts[0]->product_id} = 0;
                 }
                $cartGifts->{@$instance->gifts[0]->product_id}+=$defaultGifts;
            }
            $instance->cartGifts = $cartGifts;
        }

    }

    function buildNewCookieCart() {
        $this->postCookieCart = [];
        foreach($this->cart->instances as $x) {
            if(isset($x->product_id)) {
                if($x->product_id == $this->deletedProduct) {  continue; }
                $p = (object)['p' => $x->product_id, 'q' => $x->q];
                $this->formatGifts($x);
                $p->g = $x->cartGifts;
                $this->postCookieCart[] = $p; continue;
            }
            if(isset($x->complex_id)) {
                if($x->complex_id == $this->deletedComplex) {  continue; }
                $this->postCookieCart[] = (object)['p' => $x->complex_id, 'q' => $x->q, 't' => $x->t]; continue;
            }
        }
        $updated_at = date('Y-m-d H:i:s');
        $data = ['order_data' => \json_encode($this->postCookieCart), 'updated_at' => $updated_at];
        \Html::$primaryKey = 'tmp_order_id';
        if(self::$currentOrderId) {
            $update = \DB::table('tmp_orders')->where('tmp_order_id', self::$currentOrderId)
            ->update($data);
            if(!$update) { self::$currentOrderId = \DB::table('tmp_orders')->insertGetId($data); }
        } else {
           self::$currentOrderId = \DB::table('tmp_orders')->insertGetId($data);
        }
        self::$currentOrder = (object)$data;
        self::$currentOrder->tmp_order_id = self::$currentOrderId;
        $this->cart->hash = md5(\json_encode(self::$currentOrder->updated_at));
        session(['currentOrderId' => ShopModel::$currentOrderId]);
    }

    function getCartComplexById($id) {
        $products = \DB::select("
           SELECT products.title,  products.brutto, products.netto, concat((SELECT getProductPathById(products.product_id)), '/', products.alias) as link, ".ProductsModel::getCoalesce()." as priceinfo, products.product_id, products.avatar, 
           product_complexes.discount, product_complex_consistance.complex_id, product_complex_consistance.ordering,
           products.width, products.height, products.length, products.netto, products.brutto FROM products,
           product_complexes, product_complex_consistance WHERE 
           product_complex_consistance.complex_id = ".$id." AND product_complexes.complex_id = ".$id."
           AND products.status = true AND products.sells = true AND 
           product_complex_consistance.product_id = products.product_id
        ");
        if(count($products) < 2) { return 0; }
        return ProductsModel::handleSqlComplexes($products);
    }

    function selectGift(&$product) {
        $this->selectedGift = 0;
        if($product->gifts) {
            if($this->data['gift']) {
                foreach($product->gifts as $k => $x) {
                    if($x->product_id != $this->data['gift']) { continue; }
                    $this->selectedGift = $this->data['gift'];
                    $this->selectedGiftIndex = $k;
                    break;
                }
            }
            if(!$this->selectedGift) {
                $this->selectedGift = $this->data['gift'] = $product->giftsData->products[0];
                $this->selectedGiftIndex = 0;
            }
        }
    }

    function addProductToCart() {
        $this->init();
        $this->fields = ['product_id', 'quantity', 'gift'];
        $this->result = new \Result;
        $this->collectData();
        $this->getCart();
        if(!$this->validateProduct()) { return $this->result; }
        $product = $this->getCartProductById($this->data['product_id']);
        $this->selectGift($product);
        if(!$product) { $this->result->display = 'Извените, такого товара уже не существует'; return $this->result; }
        calculateProductPrice($product);
        $product->q = $this->data['quantity'];
        if(!$product->price || !$product->discount_price) { $this->result->display = 'Этот товар временно не доступен'; return $this->result; }
        if(isset($this->cartProductsById[$product->product_id])) {
            $newquantity = (int)$this->cartProductsById[$product->product_id]->q+(int)$this->data['quantity'];
            if($newquantity > $this->productsLimit) { $this->result->display = 'Достигнут максимум для этого товара'; return $this->result; }
            $this->cartProductsById[$product->product_id]->q = $newquantity;
            if($this->selectedGift) {
                $this->cartProductsById[$product->product_id]->cartGifts->{$this->selectedGift} = @(int)$this->cartProductsById[$product->product_id]->cartGifts->{$this->selectedGift} + $this->data['quantity'];
            }
        } else {
            if($this->selectedGift) {
                $product->cartGifts = (object)[$this->selectedGift => $this->data['quantity']];
            }
            $this->cartProductsById[$product->product_id] = $product;
            $this->cart->instances[] = &$this->cartProductsById[$product->product_id];
        }

        $this->buildNewCookieCart();
        $this->result->result = 1;
        $this->result->display = 'Товар добавлен в корзину';
        $this->recalculateCart();
        $this->cartViews();
        $this->result->cart = $this->cart;
        return $this->result;
    }

    function setProductToCart() {
        $this->init();
        $this->fields = ['product_id', 'quantity', 'gift'];
        $this->result = new \Result;
        $this->collectData();
        $this->getCart();
        if(!$this->validateProduct()) { return $this->result; }
        $product = $this->getCartProductById($this->data['product_id']);
        $this->selectGift($product);
        if(!$product) { $this->result->display = 'Извените, такого товара уже не существует'; return $this->result; }
        calculateProductPrice($product);
        $product->q = $this->data['quantity'];
        if(!$product->price || !$product->discount_price) { $this->result->display = 'Этот товар временно не доступен'; return $this->result; }
        if(isset($this->cartProductsById[$product->product_id])) {
            $newquantity = (int)$this->data['quantity'];
            if($newquantity > $this->productsLimit) { $newquantity = $this->productsLimit; }
            $this->data['quantity'] = $newquantity;
            $this->cartProductsById[$product->product_id]->q = $newquantity;
            if($this->selectedGift) {
                $this->cartProductsById[$product->product_id]->cartGifts->{$this->selectedGift} = @(int)$this->cartProductsById[$product->product_id]->cartGifts->{$this->selectedGift} + $this->data['quantity'];
            }
        } else {
            if($this->selectedGift) {
                $product->cartGifts = (object)[$this->selectedGift => $this->data['quantity']];
            }
            $this->cartProductsById[$product->product_id] = $product;
            $this->cart->instances[] = &$this->cartProductsById[$product->product_id];
        }


        $this->buildNewCookieCart();
        $this->result->result = 1;
        $this->result->display = 'Количество изменено';
        $this->recalculateCart();
        $this->cartViews();
        $this->result->cart = $this->cart;
        return $this->result;
    }

    function setComplexToCart() {
        $this->init();
        $this->fields = ['complex_id', 'quantity'];
        $this->result = new \Result;
        $this->collectData();
        $this->data['quantity'] = (int)$this->data['quantity'];
        $complex = $this->getCartComplexById($this->data['complex_id']);
        if(!$complex) { $this->result->display = 'Не удалось найти комлпект'; return $this->result; }
        if(count($complex) != 1) { $this->result->display = 'Не удалось найти комплект'; return $this->result; }
        $this->cookieCart = $this->getCookieCart();
        if($this->data['quantity'] > $this->productsLimit) { $this->data['quantity'] = $this->productsLimit; }
        if($this->data['quantity'] < 1) { $this->data['quantity'] = 1; }
        if(isset($this->cookieCartComplexesById[$complex[0]->complex_id])) {
            $newquantity = $this->data['quantity'];
            $this->cookieCartComplexesById[$complex[0]->complex_id]->q = $newquantity;
        } else {
            $this->cookieCartComplexesById[$complex[0]->complex_id] = (object)['p' => $complex[0]->complex_id, 't' => 'c', 'q' => $this->data['quantity']];
            $this->cookieCart[] = &$this->cookieCartComplexesById[$complex[0]->complex_id];
        }
        $this->postCookieCart = $this->cookieCart;
        $this->getCart();
        $this->buildNewCookieCart();
        $this->recalculateCart();
        $this->result->cart = $this->cart;
        $this->cartViews();
        $this->result->display = 'Количество изменено';
        $this->result->result = 1;
        return $this->result;
    }

    function buyInClick() {
        $this->result = new \Result;
        $this->fields = ['min_call_time', 'max_call_time', 'phone', 'product_id'];
        $this->collectData();
        $this->data['user_id'] = logged ? logged : 0;
        if(!mb_strlen($this->data['phone'])) { $this->result->display = 'Укажите ваш номер телефона'; return $this->result; }
        if(!validatePhone($this->data['phone'])) { $this->result->display = 'Неправильный номер телефона'; return $this->result; }
        if($this->data['min_call_time']) {
            try {
                $carbonFrom = @\Carbon\Carbon::createFromFormat('H:i', $this->data['min_call_time']);
            } catch(\Exception $e) {
                $this->result->display = 'Неправильный формат даты'; return $this->result;
            }
            if(!validateTime($this->data['min_call_time'])) { $this->result->display = 'Неправильный формат времени'; return $this->result; }
        }
        if($this->data['max_call_time']) {
            try {
                $carbonTo = @\Carbon\Carbon::createFromFormat('H:i', $this->data['max_call_time']);
            } catch(\Exception $e) {
                $this->result->display = 'Неправильный формат времени'; return $this->result;
            }
            if(!validateTime($this->data['max_call_time'])) { $this->result->display = 'Неправильный формат времени'; return $this->result; }
        }
        if(!\anInt($this->data['product_id'])) { $this->result->display = 'Неправильный формат данных'; return $this->result; }
        $this->data['user_id'] = logged ? logged : 0;
        $this->data['ip'] = $_SERVER['REMOTE_ADDR'];
        $todayOffers = \DB::table('singleton_offers')->where('ip', $this->data['ip'])
        ->whereRaw(\DB::raw('created_at > current_date - 1'))->count();
        if($todayOffers > 10) { $this->result->display = 'Вы уже отправили максимальное количество заявок сегодня'; return $this->result; }
        \DB::table('singleton_offers')->insert($this->data);
        $this->result->result = 1;
        return $this->result;
    }

    function activateCoupon() {
        $result = new \Result;
        $code = \Request::get('coupon');
        if(!$code) { $result->display = 'Укажите код купона'; return $result; }
        $coupon = \DB::table('coupons')->where('coupon_code', $code)->first();
        if(!$coupon) { $result->display = 'Неверный код купона'; return $result; }
        if($coupon->coupon_type == 1 && $coupon->used) { $result->display = 'Купон с таким кодом уже был использован'; return $result; }
        if($coupon->coupon_expire && strtotime(date('Y-m-d H:i:s')) > strtotime($coupon->coupon_expire)) {
            $result->display = 'Срок действия купона истек'; return $result;
        }
        \Cookie::queue(\Cookie::make('coupon', $coupon->coupon_code, time()+2678400));
        $result->coupon = $coupon;
        $result->display = 'Купон активирован';
        $result->result = 1;
        return $result;
    }

    function handleCartProduct(&$item) {
        if($item->product_id == $this->deletedProduct) { unset($item); return; }
        calculateProductPrice($item);
        if($item->priceinfo->final_price < 0.1) { unset($item); return; }
        handleProductLink($item);
        $item->total_price = $item->priceinfo->final_price*$item->q;
        $item->total_base_price = $item->priceinfo->price*$item->q;
        $item->display = new \stdClass;
        $item->display->thumbnail = \Html::productThumbnailPath($item->avatar);
        $item->display->final_price = number_format($item->priceinfo->final_price) . $this->cart->currency->sign;
        $item->display->total_price = number_format($item->total_price) . $this->cart->currency->sign;
        $item->display->discount_value = number_format($item->priceinfo->discount_value) . $this->cart->currency->sign;
        $this->instanceToTotals($item);
    }

    function handleCartComplex(&$item) {
        if($item->complex_id == $this->deletedComplex) { unset($item); return; }
        if($item->priceinfo->final_price < 0.1) { unset($item); return; }
        $item->total_price = $item->priceinfo->final_price*$item->q;
        $item->total_base_price = $item->priceinfo->final_price*$item->q;
        $item->display = new \stdClass;
        $item->display->final_price = number_format($item->priceinfo->final_price) . $this->cart->currency->sign;
        $item->display->total_price = number_format($item->total_price) . $this->cart->currency->sign;
        $item->display->discount_value = number_format($item->priceinfo->discount_value) . $this->cart->currency->sign;
        $item->q = $this->cartComplexesByIds[$item->complex_id]->q;
        $this->instanceToTotals($item);
    }

    function instanceToTotals(&$item) {
        $this->cart->totals->base_price+=$item->priceinfo->price*$item->q;
        $this->cart->totals->final_price+=$item->total_price;
        $this->cart->totals->instances+=$item->q;
        DeliveryModel::reformatProductParams($item);
        $this->cart->totals->width += ($item->width ? $item->width : defaultWidth);
        $this->cart->totals->height += ($item->height ? $item->height : defaultHeight);
        $this->cart->totals->length += ($item->length ? $item->length : defaultLength);
        $this->cart->totals->weight += ($item->weight ? $item->weight : defaultWeight);
        $this->cart->totals->discount+=$item->priceinfo->discount_value*$item->q;
        $this->cart->totals->base_discount+=$item->priceinfo->base_discount*$item->q;
        $this->cart->totals->coupon_discount+=$item->priceinfo->coupon_discount*$item->q;
        $this->cart->totals->final_discount+=($item->priceinfo->coupon_discount*$item->q) + ($item->priceinfo->base_discount*$item->q);
        if(!empty($item->giftsData)) {

            $this->cart->totals->gifts += $item->q;
        }
    }

    function getUserOrders($userId) {
        $this->result = new \Result;
        $this->result->params = [
            'p' => (int)\Request::get('p'),
            'perPage' => $this->perPage
        ];
        $this->result->params['p'] = ($this->result->params['p'] < 1) ? $this->result->params['p'] : 1;
        $this->q = \DB::table('orders')->where('user_id', $userId);
        $this->result->total = $this->q->count();
        $this->result->list = $this->q->take($this->perPage)->skip($this->params['p']*$this->perPage)->get();
        if($this->result->list->count()) {
            foreach($this->result->list as $k => $v) {

                $this->completeOrderData($this->result->list[$k]);
                $this->result->list[$k]->avatar = '';
                foreach($this->result->list[$k]->completeinfo->instances as $instance) {
                    if(@$instance->display->avatar) {
                        $this->result->list[$k]->avatar = $instance->display->avatar;
                        break;
                    }
                    if(@$instance->products[0]->avatar) {
                        $this->result->list[$k]->avatar = $instance->products[0]->avatar;
                        break;
                    }
                }
            }
        }
        return $this->result;
    }

    function extractUserFields($order) {
        $fields = (object)$order->userinfo;
        $fields->city = getCityById($order->userinfo->city);
        return $fields;
    }

    function getPdfContract($order) {
        $sm = SettingsModel::getInstance();
        $website = $sm->shareSiteData();
        $template = $order->userinfo->user_type == 1 ? 'company' : 'personal';
        $pdf =  \PDF::setOptions(['isRemoteEnabled' => true])->loadView('shop.documents.contract.index', compact('order', 'template', 'defaultCity'))->stream();
        return $pdf;
    }

    function recalculateCart() {
        $this->clearCartStats();
        if(!empty($this->cart->instances)) {
            foreach($this->cart->instances as $k => $x) {
                if(isset($this->cart->instances[$k]->product_id)) { $this->handleCartProduct($this->cart->instances[$k]); continue; }
                if(isset($this->cart->instances[$k]->complex_id)) { $this->handleCartComplex($this->cart->instances[$k]); continue; }
            }
        }
        $this->cart->display = (object)[
            'price' => number_format(round($this->cart->totals->final_price)) .' '. rusificate($this->cart->totals->final_price, 'рубль', 'рубля', 'рублей'),
            'discount' => number_format(round($this->cart->totals->discount)) .' '. rusificate($this->cart->totals->discount, 'рубль', 'рубля', 'рублей'),
            'products' => $this->cart->totals->instances.' '. rusificate($this->cart->totals->instances, 'товар', 'товара', 'товаров')
        ];
        $this->cart->hash = md5(\json_encode(@self::$currentOrder->updated_at));
    }

    function deleteFromCart() {
        $this->init();
        $this->fields = ['p', 't'];
        $this->result = new \Result;
        $this->collectData();
        if(!\anInt($this->data['p'])) { $this->result->display = 'Неправильный формат данных'; return $this->result; }
        if(@$this->data['t'] == 'c') { $this->deletedComplex = $this->data['p']; $this->result->display = 'Комплект удален'; }
        if(!@$this->data['t']) { $this->deletedProduct = $this->data['p']; $this->result->display = 'Товар удален'; }
        $this->getCart();
        $this->buildNewCookieCart();
        $this->result->cart = $this->cart;
        $this->cartViews();
        $this->result->result = 1;
        return $this->result;
    }

    function cartViews() {
        $this->result->inner = view('elements.shop.cart-inner', ['cart' => $this->cart])->render();
        if(\Request::get('cartPage')) {
            $this->result->cartPageInner = view('elements.shop.cart-page-inner', ['cart' => $this->cart])->render();
            $this->result->cartPageSide = view('elements.shop.cart-page-side', ['cart' => $this->cart])->render();
        }
    }

    function getCartProductById($id) {
        $product = \DB::table('products')
        ->select(\DB::raw("products.title, concat((SELECT getProductPathById(products.product_id)), '/', products.alias) as link, ".ProductsModel::getCoalesce()." as priceinfo,
         products.product_id, products.avatar, products.gifts, products.width, products.height, products.length, products.netto, products.brutto"))
        ->where('products.product_id', $id)
        ->where('products.status', true)
        ->first();
        if(!$product) { return 0; }
        ProductsModel::getProductGifts($product);
        return $product;
    }

    static function getSpecials($product = 0, $categories = 0) {
        $ordering = array_random(['products.title', 'products.product_id', 'products.updated_at', 'products.ordering', 'products.avatar',
         'products.description', 'products.manufacturer_id']);
        $ad = array_random(['asc', 'desc']); // asc-desc
        $specials = [];
        $toTake = 2;
        $ids = [];
        $q = \DB::table('products')
            ->select(\DB::raw("products.title, concat((SELECT getProductPathById(products.product_id)), '/', products.alias) as link, products.avatar, products.product_id, ".ProductsModel::getCoalesce()." as priceinfo"))
            ->where('products.special', true)
            ->whereNotNull('products.title')
            ->whereNotNull('products.avatar')
            ->where('products.quantity', '>', 1)
            ->where('products.status', true)
            ->where('products.sells', true)
            ->limit($toTake)->orderBy($ordering, $ad);
        if($product) { $ids[] = $product; }
        if(!empty($categories)) {
            $specials = (clone $q)
                ->join('product_categories', 'products.product_id', '=', 'product_categories.product_id')
                ->whereIn('product_categories.category_id', $categories)
                ->get();
            $toTake -= $specials->count();
            if($toTake) {
                foreach($specials as $x) {
                    $ids[] = $x->product_id;
                }
            }
        }
        if(!$toTake) { return $specials; }
        $skip = rand(0, 50);
        $altSpecials = $q->skip($skip)->limit($toTake)->get();
        if(!$altSpecials->count()) { return $specials; }
        foreach($altSpecials as $x) {
            $specials[] = $x;
        }
        return  $specials;
    }

    function postExternalProduct() {
        $this->result = new \Result;
        $this->fields = ['email', 'first_name', 'phone', 'offer_link', 'product_id', 'comment', 'ip'];
        $this->collectData();
        $this->data['ip'] = $_SERVER['REMOTE_ADDR'];
        $this->data['user_id'] = logged ? logged : 0;
        if(!$this->validateExternalProduct()) { return $this->result; }
        $externalOffersCount = \DB::table('external_offers')->where('ip', $this->data['ip'])
            ->whereRaw(\DB::raw('created_at > current_date - 1'))->count();
        if($externalOffersCount > 5) { $this->result->display = 'Вы уже отправили максимальное количество заявок сегодня.'; return $this->result; }
        try {
            \DB::table('external_offers')
                ->insert($this->data);
        } catch(\PDOException $e) {
            $this->result->display = 'Извените, произошла неизвестная ошибка, попробуйте позже'; return $this->result;
        }


        $this->result->result = 1;
        $this->result->display = 'Спасибо за ваше обращение. Наш менеджер свяжется с вами в ближайшее время';
        return $this->result;
    }

    function validateExternalProduct() {
        $nameLength = mb_strlen($this->data['first_name']);
        $emailLength = mb_strlen($this->data['email']);
        if(!$emailLength) { $this->result->display = 'Укажите адрес электронной почты'; return 0; }
        if(!$nameLength) { $this->result->display = 'Укажите ваше имя'; return 0; }
        if($nameLength > 25) { $this->result->display = 'Длина имени не должна превышать 25 символов'; return 0; }
        if($emailLength > 100) { $this->result->display = 'Длина емайла не должна превышать 100 символов'; return 0; }
        if(!isRus($this->data['first_name'])) { $this->result->display = 'Пожалуйста укажите ваше имя на русском языке'; return 0; }
        if(!mb_strlen($this->data['phone'])) { $this->result->display = 'Введите номер телефона'; return 0; }
        $linkLength = mb_strlen($this->data['offer_link']);
        if(!$linkLength) { $this->result->display = 'Укажите ссылку на товар'; return 0; }
        if(mb_strlen($this->data['comment']) > 2000) { $this->result->display = 'Длина комментария не должна превышать 3000 символов'; return 0; }
        if(!validateEmail($this->data['email'])) { $this->result->display = 'Некорректный емайл'; return 0; }
        if(!validatePhone($this->data['phone'])) { $this->result->display = 'Неправильный номер телефона'; return 0; }
        if($linkLength < 6) { $this->result->display = 'Ссылка на товар слишком короткая'; return 0; }
        if(!\anInt($this->data['product_id'])) { $this->result->display = 'Неправильный формат данных'; return 0; }
        return 1;
    }

    static function getCouponByCode($code) {
        $coupon = \DB::table('coupons')
            ->where('coupon_code', $code)
            ->whereRaw(\DB::raw("(coupon_expire < '".date('Y-m-d H:i:s')."' OR coupon_expire is null)"))
            ->first();
        return $coupon;
    }

    static function getActiveCoupon() {
        $couponCode = \Cookie::get('coupon');
        if(!$couponCode) { return 0; }
        $coupon = self::getCouponByCode($couponCode);
        if(!$coupon) {
            \Cookie::queue(\Cookie::forget('coupon'));
            return 0;
        }
        $coupon->regex = '';
        switch($coupon->discount_type) {
            case 1: $coupon->regex = ''; break; // число
            default: $coupon->regex = '/100*(100-'.$coupon->discount_value.')';
                self::$discountCalculated = $coupon->discount_value;
                break;
        }

        return $coupon;
    }

    static function calculateDiscount() {
        if(self::$discountCalculated) { return; }
        $groupDiscount = self::getGroupDiscount(user_group_id);
        if($groupDiscount) { self::$shopperGroup = $groupDiscount; }
        self::$coupon = self::getActiveCoupon();
    }

    static function getGroupDiscount($usergroup) {
        if(!$usergroup) { return 0; }
        $gd = \DB::table('user_groups_discounts')->where('user_group_id', $usergroup)->first();
        if($gd->count()) { return $gd; }
        return 0;
    }

    function setCartProductQty() {
        $this->result = new \Result;
        $this->fields = ['product_id', 'qty'];
        $this->collectData();
        if(!$this->validateProduct()) { return $this->result; }
        $this->getCart();


    }

    function validateCheckout() {
      $this->fields = ['products', 'first_name', 'last_name',
        'third_name', 'address', 'city', 'phone',
        'post_index', 'usertype', 'company_name', 'legal_address',
        'checking_account', 'bank_name', 'kpp', 'okpo', 'bik',
        'inn', 'correspond_account', 'trusted_person', 'company_director',
        'company_director_based_on'
      ];
      $this->collectData();
    }



    function calculateData() {
        $this->result();
        $this->getCart();
        $this->result->cart = $this->cart;
        $this->result->result = 1;
        $this->cartViews();
        return $this->result;
    }

    function validateCalculationData() {
        if($this->data['city_id'] != \Html::$city->city_id) {
            \Html::$city = getCityById($this->data['city_id']);
            if(!\Html::$city) { $this->result->display = 'Такого города не существует'; return 0; }
        }
        return 1;
    }

    function orderInstanceToCookieInstance(&$instance) {
        return (object)['p' => $instance->instanceId, 'q' => $instance->quantity, 'g' => $instance->gifts];
    }

    function validateCalculationInstance(&$instance) {
        $instance = (object)$instance;
        if(empty($instance->quantity)) { $this->result->display = invalidDataFormat.' #303'; $this->result->a = $instance; return 0; }
        if(!\anInt($instance->quantity)) { $this->result->display = invalidDataFormat.' #304'; return 0; }
        if(empty($instance->instanceId)) { $this->result->display = invalidDataFormat.' #305'; return 0; }
        if(!\anInt($instance->instanceId)) { $this->result->display = invalidDataFormat.' #306'; return 0; }
        if(empty($instance->priceinfo)) { $this->result->display = invalidDataFormat.' #307'; return 0; }
        $instance->priceinfo = (object)$instance->priceinfo;
        if(!Priceinfo::validatePriceinfo($instance->priceinfo)) { $this->result->display = invalidDataFormat.' #308'; return 0; }
        return 1;
    }

    function getPaymentMethods() {
        if($this->paymentMethods) { return $this->paymentMethods; }
        return $this->paymentMethods = [
            (object)['title' => 'Безналичный рассчет'],
            (object)['title' => 'Счет/Квитанция']
        ];
    }

    function validateOrder() {
        $this->result();
        $this->getCart();
        if(empty($this->cart->instances)) {
            $this->result->display = 'Выберите хотя бы один товар'; return $this->result;
        }
        $this->fields = ['user', 'delivery', 'order_city', 'payment'];
        $this->collectData();

        $sm = SettingsModel::getInstance();
        $sm->define();
        $this->data['user'] = (array)\json_decode($this->data['user']);
        $usersModel = UsersModel::getInstance();
        $usersModel->data = $this->data['user'];
        $usersModel->extra = 1;
        $usersModel->required = 1;
        $usersModel->result = new \Result;
        $usersModel->collected = 1;
        if(!$usersModel->validateRequiredProfileData()) { return $usersModel->result; }
        $this->data['user']['fullname'] = $this->data['user']['last_name'] . ' ' . $this->data['user']['first_name'] . ' ' .$this->data['user']['third_name'];
        $alterData = $this->data;
        $this->data['city_id'] = $alterData['user']['city'];
        if(!$this->validateCalculationData()) { return $this->result; }
        $this->data['delivery'] = @$alterData['delivery'];
        if(!$this->validateDeliveries()) { return $this->result; }
        $this->data['payment'] = $alterData['payment'];
        if(!$this->validatePaymentMethod()) {
            $this->data['payment'] = 0;
        }
        $paymentsModel = new PaymentsModel;
        $this->result->payment = $this->data['payment'];

        if(!$this->result->payment) {
            $paymentsModel->hash = $this->hash;
            $paymentsModel->order = (object)['tmp_order_id' => self::$currentOrderId, 'price' => $this->cart->totals->final_price];
            $this->result->paymentUrl = $paymentsModel->makePaymentLink();
        } else {
            if(!$this->createOrder()) { return $this->result; }
        }
        $this->result->cart = $this->cart;
        $this->result->result = 1;
        $this->result->coupon = self::$coupon;
        return $this->result;
    }

    function validatePaymentMethod() {
        $this->getPaymentMethods();
        if($this->data['payment'] == null || $this->data['payment'] == '') {
            $this->data['payment'] = 0;
        }
        if(!isset($this->paymentMethods[$this->data['payment']])) {
            return 0;
        }
        return 1;
    }



    function getHash() {
        $a = rand(0, 30);
        $b = rand(0, 30);
        $c = substr(md5(microtime()),0, 2);
        $d = substr(md5(microtime()),15, 17);
        $e = rand(0, 9999);
        return $c.substr(md5($c), $a, $a+4).md5($e).substr(md5($e), $b, $b+7).$d;
    }







    function getOrderByHash($hash) {
        return \DB::table('orders')->where('hash', $hash)->first();
    }

    function createOrder() {
        $dm = DeliveryModel::getInstance();
        $instances = \json_encode($this->cart->instances);
        $this->attempts = 0;
        $this->data = [
            'city_from' => DeliveryModel::$shopPoint->city_id,
            'city_to' => DeliveryModel::$city->city_id,
            'instances' => $instances,
            'user_id' => logged,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'userinfo' => \json_encode($this->data['user']),
            'price' => $this->cart->totals->final_price,
            'delivery' => $this->data['delivery'][0],
            'deliveryinfo' => $dm->deliveryLinks[$this->data['delivery'][0]]->title,
            'completeinfo' => \json_encode($this->cart),
            'free_delivery_limit' => freeDeliveryLimit,
            'password' => str_random(6),
            'coupon' => self::$coupon ? \json_encode(self::$coupon) : null,
            'coupon_code' => self::$coupon ? self::$coupon->coupon_code : null,
            'payment_method' => $this->data['payment']
        ];
        \Html::$primaryKey = 'order_id';
        if($this->hash) {
            if(!$this->updateOrder()) { return 0; }
            $this->data['hash'] = $this->hash;
        } else {
            if(!$this->insertOrder()) { return 0; }
        }
        if($this->result->order) {
            \DB::statement("select tsvector_order_data(".$this->result->order.")");
        }
        $this->result->link = logged ? '/my/orders/'.$this->data['hash'] : '/order/'.$this->data['hash'];
        $this->result->hash = $this->data['hash'];
        if(self::$coupon) {
            $this->useCoupon(self::$coupon->coupon_code);
        }
        $this->insertedOrder = $this->getOrderByHash($this->data['hash']);
        $this->completeOrderData($this->insertedOrder);
        //$mailerModel->sendFormedOrder($mailOrder);

        $this->purgeCart();
        return 1;
    }

    function updateOrder() {
        if($this->attempts > 50) {
            $this->result->display = 'Извените, произошла неизвестная ошибка';
            return 0;
        }
        try {
            unset($this->data['hash']);
            $this->result->order =  \DB::table('orders')->where('hash', $this->hash)->update($this->data);
        } catch(\PDOException $e) {
            $this->attempts++;
            return  $this->updateOrder();
        }
        return 1;
    }

    function completeOrderData(&$order) {
        $order->city_to = getCityById($order->city_to);
        $order->city_from = getCityById($order->city_from);
        $order->completeinfo = \json_decode($order->completeinfo);
        $order->userinfo = \json_decode($order->userinfo);
        $order->instances = \json_decode($order->instances);
        $this->fillOrderInstancesWithCodes($order->completeinfo->instances);
        $order->coupon = \json_decode($order->coupon);
        $order->created_at = \DateFormatter::getDateObject($order->created_at);
        if($order->user_id) {
            $order->userinfo->username = \DB::table('users')->where('id', $order->user_id)->select('username')->first();
        }
        $usersModel = UsersModel::getInstance();
        $order->userinfo->user_type_title = $usersModel->getUserTypes()[$order->userinfo->user_type]->title;
        $order->userinfo->company_director_title = @$usersModel->usertypes[1]->fields[9]->values[$order->userinfo->company_director];
        $order->userinfo->company_director_based_on_title = @$usersModel->usertypes[1]->fields[10]->values[$order->userinfo->company_director_based_on];
        $order->title = ($order->userinfo->user_type == 1 ? 'ДОГОВОР ПОСТАВКИ' : 'ДОГОВОР КУПЛИ - ПРОДАЖИ').' L-'.$order->order_id;
        $order->template = $order->userinfo->user_type == 1 ? 'company' : 'personal';
        $order->status_title = $this->getOrderStatuses()[$order->status]->title;
        $order->payment_method_title = $this->getPaymentMethods()[$order->payment_method]->title;
        $deliveryModel = DeliveryModel::getInstance();
        $order->delivery_title = @$deliveryModel->getDeliveryLinks()[$order->delivery];
        $this->reformatInstancesAvatars($order->completeinfo);
    }

    function fillOrderInstancesWithCodes(&$orderInstances) {
        if(empty($orderInstances)) { return; }
        $ids = [];
        foreach($orderInstances as $instance) {
            if(@$instance->t == 'c') {
                foreach($instance->products as $k => $v) {
                    $ids[] = $v->product_id;
                }
                continue;
            }
            // handle products
            $ids[] = $instance->product_id;
            if(!empty($instance->gifts)) {
                foreach($instance->gifts as $k => $v) {
                    $ids[] = $k;
                }
            }
        }
        $ids = array_unique($ids);
        if(empty($ids)) { return; }
        $codes = \DB::table('product_codes')->whereIn('product_id', $ids)->get();
        if(!$codes) { return; }
        $this->codesByProducts = \arrayMap($codes, 'product_id', 1);
        foreach($orderInstances as $k => $instance) {
            if(@$instance->t == 'c') {
                foreach($instance->products as $pk => $prod) {
                    $this->assignCodesToProduct($orderInstances[$k]->products[$pk]);
                }
                continue;
            }
            $this->assignCodesToProduct($orderInstances[$k]);
            if(empty($orderInstances[$k]->giftsById)) { continue; }
            foreach($orderInstances[$k]->giftsById as $giftK => $v) {
                $this->assignCodesToProduct($orderInstances[$k]->giftsById->{$giftK});
            }
        }

    }

    function assignCodesToProduct(&$product) {
        $product->codes = [];
        $product->codes_string = '';
        if(!isset($this->codesByProducts[$product->product_id])) { return; }
        $comma = '';
        foreach($this->codesByProducts[$product->product_id] as $k => $code) {
            $product->codes[] = $code->product_code;
            $product->codes_string .= $comma.$code->product_code;
            $comma = ', ';
        }
    }
   
    function getOrderStatuses() {
        if($this->orderStatuses) { return $this->orderStatuses; }
        return $this->orderStatuses = [
              1 => (object)['title' => 'Ожидает оплаты'],
              2 => (object)['title' => 'Поступила оплата'],
              3 => (object)['title' => 'Заказ оплачен'],
              4 => (object)['title' => 'Передан на склад'],
              5 => (object)['title' => 'Передан в ТК']
        ];
    }

    function insertOrder() {
        if($this->attempts > 50) {
            $this->result->display = 'Извените, произошла неизвестная ошибка';
            return 0;
        }
        try {
            $this->data['hash'] = $this->getHash();
            $this->result->order =  \DB::table('orders')->insertGetId($this->data);
        } catch(\PDOException $e) {
            $this->attempts++;
            return  $this->insertOrder();
        }
        return 1;
    }

    function useCoupon($code) {
        \DB::table('coupons')->where('coupon_code', $code)->update(['used' => true]);
    }

    function purgeCart() {
        \DB::table('tmp_orders')->where('tmp_order_id', self::$currentOrderId)->delete();
        \Cookie::queue(\Cookie::forget('coupon'));
    }

    function validateDeliveries() {
        if(!$this->data['delivery']) { $this->result->display = 'Выберите способ доставки'; return 0; }
        $dm = DeliveryModel::getInstance();
        $dm->init();
        $dm->getDeliveryLinks();
        if($this->data['delivery'] == 1) {
            $settings = (SettingsModel::getInstance())->getSiteData();
            if(!@$settings->freeDeliveryLimit) { $this->result->display = 'На данный момент у нас отсутствует бесплатная доставка'; return 0; }
            if(@$this->cart->totals->final_price < $settings->freeDeliveryLimit) {
                $this->result->display = 'Для того, чтобы воспользоваться бесплатной доставкой вам нужно купить товаров еще на '. number_format($settings->freeDeliveryLimit-$this->cart->totals->final_price). ' р.'; return 0;
            }
            return 1;
        }
        if(!isset($dm->deliveryLinks[$this->data['delivery']])) { $this->result->display = 'Выбранного способа доставки не существует'; return 0; }
        if(@$dm->deliveryLinks[$this->data['delivery']]->address) {
            // address
            if(empty($this->data['address'])) { $this->result->display = 'Укажите адрес доставки'; return 0; }
            $length = mb_strlen($this->data['address']);
            if($length < 6) { $this->result->display = 'Адрес доставки не может быть кароче 6 символов'; return 0; }
            if($length > 100) { $this->result->display = 'Длина адреса доставки не должна превышать 100 символов'; return 0; }
            if(preg_match('/[A-z]/',$this->data['address'], $matches)) { $this->result->display = 'Введите адрес на русском языке'; return 0; }
        }
        return 1;
    }

    function reformatInstancesAvatars(&$info) {
        if(empty($info->instances)) { return; }
        $ids = [];
        foreach($info->instances as $x) {
            if(!@$x->product_id) {
                foreach($x->products as $product) {
                    $ids[] = $product->product_id;
                }
                continue;
            }
            $ids[] = $x->product_id;
            if(!empty($x->cartGifts)) {
                foreach($x->cartGifts as $cartGiftId => $cartGift) {
                    $ids[] = $cartGiftId;
                }
            }
        }

        $ids = array_unique($ids);
        if(empty($ids)) { return; }
        $newinfo = \DB::table('products')->select('avatar', 'product_id', \DB::raw("concat((SELECT getProductPathById(products.product_id)), '/', products.alias) as link"), 'title')
        ->whereIn('product_id', $ids)
        ->get();
        if(empty($newinfo)) { return; }
        $final = [];
        foreach($newinfo as $x) {
            handleProductLink($x);
            $final[$x->product_id] = $x;
        }
        foreach($info->instances as $k => $x) {
            if(!@$x->product_id) {
                foreach($x->products as $productK => $product) {
                    if(empty($final[$product->product_id])) {  continue; }
                    $info->instances[$k]->products[$productK]->link = $final[$product->product_id]->link;
                    $info->instances[$k]->products[$productK]->title = $final[$product->product_id]->title;
                }
                continue;
            }
            if(empty($final[$x->product_id])) { continue; }
            $info->instances[$k]->link = $final[$x->product_id]->link;
            $info->instances[$k]->display->thumbnail = \Html::productThumbnailPath($final[$x->product_id]->avatar);
            $info->instances[$k]->display->avatar = $final[$x->product_id]->avatar;
            $info->instances[$k]->display->title = $final[$x->product_id]->title;
            if(!empty($x->cartGifts)) {
                foreach($x->cartGifts as $cartGiftId => $cartGift) {
                    $ids[] = $cartGiftId;
                }
            }
        }
        return;
    }


    function setGifts() {
        $this->result = new \Result;
        $this->fields = ['product_id', 'giftsData'];
        $this->collectData();
        $this->init();
        $this->getCookieCart();
        $this->data['giftsData'] = \json_decode($this->data['giftsData']);
        if(!\anInt($this->data['product_id']) || !$this->data['giftsData']) { $this->result->display = invalidDataFormat; return $this->result; }
        foreach($this->cookieCart as $k => $v) {
            if($v->p != $this->data['product_id']) { continue; }
            $v->g = $this->data['giftsData'];
        }
        $this->getCart();
        $this->buildNewCookieCart();
        $this->cartViews();
        return $this->result;
    }



    function result() {
        if(!$this->result) { $this->result = new \Result; }
    }

    function calculateDeliveryCosts() {
        $this->result();
        $this->fields = ['instances', 'city_id'];
        $this->collectData();
        if(!$this->validateCalculationData()) { return $this->result; }
        $this->result->priceinfo = new Priceinfo;
        $this->result->totals = new \stdClass;
        $this->result->totals->instances = 0;
        if(!$this->validateCalculationData()) { return $this->result; }
        $this->result->postCookieCart = $this->postCookieCart;
        $this->getCart();
        $this->result->cart = $this->cart;
        $deliveryModel = DeliveryModel::getInstance();
        $this->result->costs = $deliveryModel->calculateCosts($this->cart->totals);
        $this->result->result = 1;
        return $this->result;
    }



}
