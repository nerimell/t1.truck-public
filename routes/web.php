<?php

Route::get('/', 'PagesController@mainPage');



Route::group(['middleware' => 'visitor'], function() {
    Route::get('/registration', 'UsersController@registrationPage');
    Route::get('/login', 'UsersController@loginPage');
    Route::get('/restore-password', 'UsersController@restorePage');
    Route::get('/restore-login', 'UsersController@restoreLoginPage');
});

Route::post('/reg', 'UsersController@register'); 

Route::group(['middleware' => 'auth'], function() {
    Route::get('/profile', 'UsersController@profilePage');
    Route::post('/profile', 'UsersController@saveSettings');
    Route::get('/my', 'UsersController@my');
    Route::get('/my/orders/{hash}', 'ShopController@myOrder');
});


Route::get('/my/orders', 'UsersController@myOrders');
Route::post('/location/switch', 'LocationController@change');
Route::get('/logout', 'UsersController@logout');
Route::post('/async-login', 'UsersController@asyncLogin');
Route::post('/timezone', 'CommonController@timezone');
Route::post('/buy-in-click', 'ShopController@buyInClick');
Route::post('/promotion-discount', 'ShopController@postExternalProduct');
Route::post('/cart/add', 'ShopController@addToCart');
Route::post('/cart/set', 'ShopController@setToCart');
Route::post('/cart/set-complex', 'ShopController@setComplexToCart');
Route::post('/cart/set-gifts', 'ShopController@setGifts');
Route::post('/cart/complex', 'ShopController@addComplexToCart');
Route::post('/cart/delete', 'ShopController@deleteFromCart');
Route::post('/cart/refresh', 'ShopController@refreshCart');
Route::post('/cart/validate', 'ShopController@validateCart');
Route::post('/cart/calculate-data', 'ShopController@calculateData');
Route::post('/cart/validate-order', 'ShopController@validateOrder');
Route::post('/delivery/product', 'ShopController@calculateProductDelivery');
Route::post('/delivery/cart', 'ShopController@calculateCartDelivery');
Route::get('/cart', 'ShopController@cartPage');
Route::get('/order/change/{hash}', 'ShopController@changeOrder');
Route::post('/order/getPaymentUrl', 'PaymentController@getPaymentUrl');
Route::post('/order/formed', 'ShopController@formedOrder');
Route::get('/order/{hash}', 'ShopController@hashOrderPage');
Route::post('/order/{hash}', 'ShopController@hashOrderPage');
Route::get('/payment/error', 'PaymentController@error');
Route::get('/payment/error-test', 'PaymentController@error');
Route::post('/payment/checkurl', 'PaymentController@checkOrder');
Route::get('/payment/checkurl', 'PaymentController@checkOrder');
Route::get('/thanks', 'CommonController@thanksPage');
Route::post('/checkout', 'ShopController@checkoutPage');
Route::post('/coupon/activate', 'ShopController@activateCoupon');
Route::get('/cf', 'ShopController@forgetCoupon');
Route::get('/search', 'ProductsController@search');
Route::get('/news', 'NewsController@newsPage');
Route::get('/news/{alias}', 'NewsController@postPage');

Route::post('/doubles/deletePair', 'DoublesController@deletePair');
Route::post('/doubles/intoProduct', 'DoublesController@intoProduct');
Route::get('/proizvoditel/{alias}', 'ManufacturerController@manufacturerPage');
Route::post('/user/remember-field', 'UsersController@rememberField');
Route::get('/users', 'UsersController@index');
Route::get('/ask-our-manager', 'FeedbackController@askPage');

Route::get('/contract/{hash}', 'ShopController@getContractByHash');

Route::get('/print', 'PrintController@printImage');
Route::get('/{var}-detail', 'ProductsController@productPage')->where('var', '(.*)');
Route::get('/{path}', 'CommonController@whatever')->where('path','(.*)');
